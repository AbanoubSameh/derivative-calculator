// This file has been created on June-20-2020
// Copyright (c) 2020 Abanoub Sameh

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "headers/solveDerivative.h"

Function *solveExpDer(Function *input)
{
    if (input == NULL) {
        printf("can't solve null input\n");
        return NULL;
    }

    if (ArgIsConst(input->arg2)) {
        Function *prod = new_Function();
        prod->funcType = PROD;
        prod->arg1 = new_ArgConst(input->arg2->arg.dbl);
        input->arg2->arg.dbl--;

        if (input->arg1->argType == FUNC) {
            Function *mul = new_Function();
            mul->funcType = PROD;
            prod->arg2 = new_ArgFunc(mul);
            mul->arg1 = solveDerivative(input->arg1);
            //print_Argument(mul->arg1, 1);
            mul->arg2 = new_ArgFunc(input);
            //print_Argument(mul->arg2, 1);
        } else
            prod->arg2 = new_ArgFunc(input);

        return prod;
    } else {
        if (ArgIsConst(input->arg1)
                && input->arg1->arg.dbl == M_E) {
            Function *eMul = new_Function();
            eMul->funcType = PROD;
            eMul->arg1 = new_ArgFunc(input);
            eMul->arg2 = solveDerivative(input->arg2);
            //eMul->arg2 = input->arg2;
            return eMul;
        } else {
            Function *ln = new_FuncWithValues(LOG, new_ArgConst(M_E), input->arg1);
            Function *expo = new_FuncWithValues(PROD, input->arg2, new_ArgFunc(ln));
            Function *result = new_FuncWithValues(PROD, new_ArgFunc(input),
                    solveDerivative(new_ArgFunc(expo)));
            //printf("%lx\n", (long) result);
            //print_Argument(result, 1);
            return result;
        }
    }

    return NULL;
}
