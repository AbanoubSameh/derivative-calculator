// This file has been created on June-20-2020
// Copyright (c) 2020 Abanoub Sameh

#include <stdio.h>
#include <stdlib.h>

#include "headers/solveDerivative.h"

#include "headers/solveAlgebDer.h"
#include "headers/solveExpDer.h"
#include "headers/solveLogDer.h"
#include "headers/solveTrigDer.h"

Function *solveFunctionDer(Function *input)
{
    if (input == NULL) {
        // error
        printf("** NULL FUNCTION POINTER **");
        return NULL;
    }
    if (input->funcType == NULL_FUNC) {
        // error
        printf("** NULL FUNCTION **");
        return NULL;
    }

    if (input->funcType == SUM || input->funcType == DIFF
            || input->funcType == PROD || input->funcType == QUOT)
        return solveAlgebDer(input);
    else if (input->funcType == EXP) {
        if (ArgIsConst(input->arg1) && ArgIsConst(input->arg2)) return new_FuncZero();
        else return solveExpDer(input);
    } else if (input->funcType == LOG) {
        if (ArgIsConst(input->arg1) && ArgIsConst(input->arg2)) return new_FuncZero();
        else return solveLogDer(input);
    } else if (input->funcType == TRIG) {
        if (ArgIsConst(input->arg2)) return new_FuncZero();
        return solveTrigDer(input);
    }

    return NULL;
}

Argument *solveDerivative(Argument *input)
{
    if (input == NULL) {
        // error
        printf("** CAN'T SOLVE NULL ARGUMENT POINTER **");
        return NULL;
    }
    if (input->argType == NULL_ARG) {
        // error
        printf("** NULL ARGUMENT **");
        return NULL;
    }

    //printf("input type %d\n", input->argType);
    //printf("type %d\n", CONST);

    if (ArgIsConst(input))
        return new_ArgConst(0);
    else if (input->argType == VAR)
        return new_ArgConst(1);
    else if (input->argType == FUNC)
        return new_ArgFunc(solveFunctionDer(Func_duplic(input->arg.func)));
    else {
        printf("** NO SUCH ARGUMENT TYPE **");
        return NULL;
    }
}
