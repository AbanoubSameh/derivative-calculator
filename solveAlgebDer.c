// This file has been created on June-20-2020
// Copyright (c) 2020 Abanoub Sameh

#include <stdio.h>
#include <stdlib.h>

//#include "headers/main.h"
#include "headers/solveDerivative.h"

Function *solveSum(Function *input)
{
    Function *new = new_Function();
    new->funcType = input->funcType;
    //printf("solving left side\n");
    new->arg1 = solveDerivative(input->arg1);
    //printf("solving right side\n");
    new->arg2 = solveDerivative(input->arg2);
    return new;
}

Function *solveProd(Function *input)
{
    Function *new = new_Function();
    new->funcType = SUM;
    //new->arg1 = new_ArgFunc(new_FuncWithValues(PROD,
                //solveDerivative(input->arg1), input->arg2));
    //new->arg1 = new_ArgFunc(new_FuncWithValues(PROD,
                //solveDerivative(input->arg2), input->arg1));
    new->arg1 = new_ArgValues(FUNC, new_FuncWithValues(PROD,
                solveDerivative(input->arg1), input->arg2));
    new->arg2 = new_ArgValues(FUNC, new_FuncWithValues(PROD,
                solveDerivative(input->arg2), input->arg1));
    return new;
}

Function *solveQuot(Function *input)
{
    Function *new = new_Function();
    new->funcType = QUOT;
    Function *temp = new_FuncWithValues(PROD, input->arg1, input->arg2);
    new->arg1 = new_ArgValues(FUNC, solveProd(temp));
    new->arg1->arg.func->funcType = DIFF;
    if (ArgIsConst(input->arg2) && input->arg2->arg.dbl == 0) {
        printf("** CAN'T CREATE A FUNCTION WITH ZERO DENUMERATOR **");
        //cleanup(-3);
        return NULL;
    }
    new->arg2 = new_ArgValues(FUNC, new_FuncWithValues(EXP, input->arg2, new_ArgConst(2)));
    return new;
}

Function *solveAlgebDer(Function *input)
{
    if (input->funcType == SUM || input->funcType == DIFF)
        return solveSum(input);
    else if (input->funcType == PROD)
        return solveProd(input);
    else if (input->funcType == QUOT)
        return solveQuot(input);
    return NULL;
}
