// This file has been created on June-20-2020
// Copyright (c) 2020 Abanoub Sameh

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "headers/Argument.h"
#include "headers/Function.h"
#include "headers/duplicate.h"
#include "headers/main.h"
#include "headers/Container.h"

#include "headers/LinkedList.h"

#include "headers/debug.h"

extern LinkedList *argList;

Argument *new_ArgEmpty()
{
    Argument *new = malloc(sizeof(Argument));
    //printf("new empty argument created at %ld\n", (long) new);
    new->argType = NULL_ARG;
    new->arg.func = NULL;

    add_to_start_of_LinkedList(argList, new);
    return new;
}

Argument *new_ArgValues(ARG_TYPE type, void *input)
{
    Argument *new;

    if (type == CONST)
        new = new_ArgConst(*(double*)input);
    else if (type == VAR)
        new = new_ArgVar(input);
    else if (type == FUNC)
        new = new_ArgFunc(input);
    else if (type == CONST_LETTER)
        new = new_ArgConstLetter(input);
    else
        new = NULL;

    //printf("new argument created at %ld with type %d\n", (long) new, new->argType);
    return new;
}

Argument *new_ArgConst(double dbl)
{
    Argument *new = malloc(sizeof(Argument));
    //printf("new argument created at %ld with type CONST\n", (long) new);
    new->argType = CONST;
    new->arg.dbl = dbl;

    add_to_start_of_LinkedList(argList, new);
    return new;
}

Argument *new_ArgConstLetter(char *str)
{
    char *newStr = strduplic(str);
    //printf("new string created at %ld\n", (long) newStr);
    Argument *new = malloc(sizeof(Argument));
    //printf("new argument created at %ld with type CONST_LETTER\n", (long) new);
    new->argType = CONST_LETTER;
    new->arg.str = newStr;

    add_to_start_of_LinkedList(argList, new);
    return new;
}

Argument *new_ArgVar(char *str)
{
    char *newStr = strduplic(str);
    //printf("new string created at %ld\n", (long) newStr);
    Argument *new = malloc(sizeof(Argument));
    //printf("new argument created at %ld with type VAR\n", (long) new);
    new->argType = VAR;
    new->arg.str = newStr;

    add_to_start_of_LinkedList(argList, new);
    return new;
}

Argument *new_ArgFunc(void *func)
{
    Argument *new = malloc(sizeof(Argument));
    //printf("new argument created at %ld with type FUNC\n", (long) new);
    new->argType = FUNC;
    new->arg.func = func;

    add_to_start_of_LinkedList(argList, new);
    return new;
}

Argument *new_ArgContainer(void *container)
{
    Argument *new = malloc(sizeof(Argument));
    //printf("new argument created at %ld with type CONTAINER\n", (long) new);
    new->argType = CONTAINER;
    new->arg.container = container;

    add_to_start_of_LinkedList(argList, new);
    return new;
}

void print_Argument(Argument *input, int addNewLine)
{
    //printf("printing argument\n");
    if (input == NULL) {
        // error
        printf("** CAN'T PRINT NULL ARGUMENT POINTER **");
        return;
    }

    if (input->argType == NULL_ARG) {
        // error
        printf("** NULL ARGUMENT **");
        return;
    }

    if (input->argType == CONST) {
        char temp_buffer[1000];
        snprintf(temp_buffer, 1000, "%0.9g", input->arg.dbl);
        strncat(output_buffer, temp_buffer, ob_size - strlen(output_buffer));
    } else if (input->argType == VAR || input->argType == CONST_LETTER) {
        strncat(output_buffer, input->arg.str, ob_size - strlen(output_buffer));
    } else if (input->argType == FUNC) {
        print_Function(input->arg.func, 0);
    } else if (input->argType == CONTAINER) {
        print_Container(input->arg.container, 0, 0);
    } else {
        printf("** UNKNOWN ARGUMENT TYPE **");
    }

    if (addNewLine) {
        strncat(output_buffer, "\n", ob_size - strlen(output_buffer));
    }
}

void print_Argument_out(Argument *input, int addNewLine)
{
    if (input == NULL) {
        // error
        printf("** CAN'T PRINT OUT NULL ARGUMENT POINTER **");
        return;
    }

    //printf("printing argument with type: %d\n", input->argType);

    if (input->argType == NULL_ARG) {
        // error
        printf("** NULL ARGUMENT **");
        return;
    }

    if (input->argType == CONST) {
        //printf("temp buffer: %s\n", temp_buffer);
        //printf("buffer: %s\n", output_buffer);
        printf("%0.9g", input->arg.dbl);
    } else if (input->argType == VAR || input->argType == CONST_LETTER) {
        printf("%s", input->arg.str);
    } else if (input->argType == FUNC) {
        print_Function_out(input->arg.func, 0);
    } else if (input->argType == CONTAINER) {
        print_Container_out(input->arg.container, 0, 0);
    } else {
        printf("** UNKNOWN ARGUMENT TYPE **");
    }

    if (addNewLine) {
        printf("\n");
    }
}

void *print_Argument_debug(Argument *input)
{
    if (input == NULL) {
        // error
        printf("** CAN'T PRINT OUT NULL ARGUMENT POINTER **");
        return NULL;
    }

    if (input->argType == NULL_ARG) {
        // error
        printf("** NULL ARGUMENT **");
        return NULL;
    }

    if (input->argType == CONST) {
        printf("%0.9g", input->arg.dbl);
    } else if (input->argType == VAR || input->argType == CONST_LETTER) {
        printf("%s", input->arg.str);
    } else if (input->argType == FUNC) {
        printf("f");
        print_id((long) input->arg.func);
        return input;
    } else if (input->argType == CONTAINER) {
        printf("c");
        print_id((long) input->arg.container);
        return input;
    } else {
        printf("** UNKNOWN ARGUMENT TYPE **");
    }
    return NULL;
}

void print_Argument_debug_content(Argument *input)
{
    if (input == NULL) {
        // error
        printf("** CAN'T PRINT OUT NULL ARGUMENT POINTER **");
        return;
    }

    //print_id((long) input);

    if (input->argType == NULL_ARG) {
        // error
        printf("** NULL ARGUMENT **");
        return;
    }

    if (input->argType == FUNC) {
        print_Function_debug(input->arg.func);
    } else if (input->argType == CONTAINER) {
        print_Container_debug(input->arg.container);
    } else {
        printf("** UNKNOWN ARGUMENT TYPE **");
    }
}

Argument *Arg_duplic(Argument *input)
{
    Argument *new = new_ArgEmpty();
    new->argType = input->argType;

    if (input->argType == CONST) {
        new->arg.dbl = input->arg.dbl;
    } else if (input->argType == VAR || input->argType == CONST_LETTER) {
        new->arg.str = strduplic(input->arg.str);
    } else if (input->argType == FUNC) {
        new->arg.func = Func_duplic(input->arg.func);
    } else if (input->argType == NULL_ARG) {
        new->arg.func = NULL;
    } else {
        printf("** WRONG INPUT TYPE %d **", input->argType);
    }

    return new;
}

void delete_Argument(Argument *input)
{
    if (input == NULL) {
        printf("Can't delete NULL Argument\n");
        return;
    }
    //printf("freeing an argument at: %ld\n", (long) input);
    if (input->argType == VAR || input->argType == CONST_LETTER) {
        //printf("freeing string at: %ld\n", (long) input->arg.str);
        free(input->arg.str);
        input->arg.str = NULL;
    }
    free(input);
}

int ArgIsConst(Argument *input)
{
    if (input == NULL) {
        printf("** CAN'T GET TYPE OF NULL ARGUMENT **");
        return -1;
    }

    if (input->argType == CONST || input->argType == CONST_LETTER)
        return 1;
    else
        return 0;
}
