// This file has been created on June-20-2020
// Copyright (c) 2020 Abanoub Sameh

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "headers/main.h"

#include "generated/parser.h"

#include "headers/solveDerivative.h"
#include "headers/optimizer.h"
#include "headers/Container.h"

#include "headers/LinkedList.h"

// output_buffer size factor
#define OB_SIZE_MUL     150
#define MAX_INPUT_SIZE  1500

int mycode = 0;
char *errors[] = { "Invalid character", "Syntax error", "Empty input", "Math error" };

// these keep track of dynamically created objects
LinkedList *argList;
LinkedList *funcList;
LinkedList *contList;
LinkedList *listList;

// this is created by main to hold the input string
char *stringBuffer = NULL;
// this is created by main to return the output string
// and it has to be freed be the caller
char *output_buffer = NULL;
// this keeps track of that output_buffer size
int ob_size;

// we are taking the derivative, with respect to this variable
char dvar[2];

// initialize all the needed stuff
void init()
{
    dvar[0] = 'x';
    argList = new_metaLinkedList();
    funcList = new_metaLinkedList();
    contList = new_metaLinkedList();
    listList = new_metaLinkedList();
}

// cleanup before we leave
void cleanup(int exitCode)
{
    delete_metaLinkedList(argList, ARGUMENT_T);
    delete_metaLinkedList(funcList, FUNCTION_T);
    delete_metaLinkedList(contList, CONTAINER_T);
    delete_metaLinkedList(listList, LIST_T);
    free(stringBuffer);

    if (exitCode) {
        //printf("error code %d, exiting ...\n", exitCode);
        strcpy(output_buffer, errors[-exitCode - 1]);
    }
}

// this holdes the abstract syntax tree returned by the parser
Argument *Ast;

// this is used by the lexer as input function
// this allows us to easily copy the input into a buffer for example
// or modifing it before we send it to the lexer
char *readString(char *buffer, int *num, int max_num)
{
    if (strlen(stringBuffer) < (unsigned long) max_num)
        max_num = strlen(stringBuffer);

    for (int i = 0; i < max_num; i++) {
        buffer[i] = stringBuffer[i];
        printf("%c", buffer[i]);
    }

    *num = max_num;
    return 0;
}

// this should be the main function
// it is called by the actual main
// and this allows us to use the use this program
// as a separate library
char *calc(char *input)
{
    init();

    ob_size = strlen(input) * OB_SIZE_MUL;
    if (ob_size < 50)
        ob_size = 50;
    // we use this variable to determine the buffer size
    // as a factor of the input string

    // allocate space for input and output strings
    // we will free stringBuffer, but the caller needs to free output_buffer
    stringBuffer = malloc(strlen(input) + 1);
    output_buffer = malloc(ob_size + 1);

    strcpy(stringBuffer, input);
    strcpy(output_buffer, "");

    Argument *derivative;
    Argument *optimized;

    // call the parser
    yyparse();

    if (mycode) {
        cleanup(mycode);
        return output_buffer;
    }

    /*
    printf("buffer: %s\n", stringBuffer);
    printf("input: ");
    print_Argument_out(Ast, 1);
    return output_buffer;
    */

    // optimize and then de-containerize the input first
    Argument *optInput = optimizer(Ast);

    if (optInput == NULL) {
        cleanup(-4);
        return output_buffer;
    }

    //if (optInput->argType == CONTAINER)
        //print_Container_debug(optInput->arg.container);

    Argument *temp = deContainerize(optInput);

    if (temp == NULL) {
        cleanup(-4);
        return output_buffer;
    }

    // then get the derivative
    derivative = solveDerivative(temp);

    if (derivative == NULL) {
        cleanup(-4);
        return output_buffer;
    }

    //derivative = solveDerivative(Ast);
    printf("answer: ");
    print_Argument_out(derivative, 1);
    //printf("optimizing ...\n");

    // optimize the result again
    optimized = optimizer(derivative);

    if (optimized == NULL) {
        cleanup(-4);
        return output_buffer;
    }

    //printf("done!!\n");
    if (optimized->argType == CONTAINER)
        print_Container_debug(optimized->arg.container);

    print_Argument(optimized, 0);

    //printf("size left: %ld\n", ob_size - (strlen(output_buffer) + 1));

    // cleanup
    cleanup(0);

    //system("pause");
    return output_buffer;
}

// might be called by the parser to change the variable
// with respect to which the derivative is to be taken
void change_dvar(char *newdvar)
{
    if (newdvar != NULL) {
        //dvar[0] = newdvar[0];
        dvar[0] = newdvar[0];
        dvar[1] = 0;
        printf("dvar now: %s\n", dvar);
    }
}

// tells the caller if the test string is the same as
// the variable with respect to which the derivative is to be taken
int withRespectTo(char *test)
{
    if (test == NULL || dvar[0] == 0) {
        printf("** EMPTY CONSTANT OR DERIVATION VALUE NOT SPECIFIED **\n");
        return -1;
    }

    if (!strcmp(test, dvar))
        return 1;
    else
        return 0;
}

int main()
{
    char temp[MAX_INPUT_SIZE];
    printf("please enter you equation\n");
    fgets(temp, MAX_INPUT_SIZE - 2, stdin);
    strcat(temp, "\n");

    char *out = calc(temp);

    printf("output: \"%s\"\n", out);
    free(out);
}
