// This file has been created on June-30-2020
// Copyright (c) 2020 Abanoub Sameh

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "headers/optimizer.h"
#include "headers/Argument.h"
#include "headers/Function.h"
#include "headers/solveTrigDer.h"
#include "headers/LinkedList.h"
#include "headers/Container.h"
#include "headers/main.h"
#include "headers/duplicate.h"

int sameArg(Argument *, Argument *);
int sameFunc(Function *, Function *);
int sameContainer(Container *, Container *);

// tells the caller if two arguments are identical
int sameArg(Argument *arg1, Argument *arg2)
{
    // return false if the arguments don't have the same type
    if (arg1->argType != arg2->argType)
        return 0;

    if (arg1->argType == CONST) {
        // if double is the arguments' type check if the numbers are equal
        if (arg1->arg.dbl == arg2->arg.dbl)
            return 1;
    } else if (arg1->argType == VAR || arg1->argType == CONST_LETTER) {
        // if variable is the arguments' type check if the variables are the same
        if (arg1->arg.str[0] == arg2->arg.str[0])
            return 1;
    } else if (arg1->argType == FUNC) {
        // if function is the arguments' type check if the functions are the same
        if (sameFunc(arg1->arg.func, arg2->arg.func))
            return 1;
    } else if (arg1->argType == CONTAINER) {
        // if container is the arguments' type check if the containers are the same
        if (sameContainer(arg1->arg.container, arg2->arg.container))
            return 1;
    } else {
        printf("** UNKNOWN ARGUMENT TYPE **");
    }

    return 0;
}

// tells the caller if two functions are identical
int sameFunc(Function *func1, Function *func2)
{
    // return false if the functions don't have the same type
    if (func1->funcType != func2->funcType)
        return 0;

    if (func1->funcType == EXP || func1->funcType == LOG) {
        // it the function have arg1 and arg2, check if both are identical
        if (sameArg(func1->arg1, func2->arg1) && sameArg(func1->arg2, func2->arg2))
            return 1;
    } else if (func1->funcType == TRIG) {
        // trig functions have only arg2, and arg1 is just the function type
        // i.e. sin, cos, tan ..
        if (func1->arg1 == func2->arg1 && sameArg(func1->arg2, func2->arg2))
            return 1;
    } else {
        printf("** UNKNOWN FUNCTION TYPE **");
    }

    return 0;
}

// tells the caller if two containers are identical
int sameContainer(Container *container1, Container *container2)
{
    // return false if the container types, or if the number of arguments
    // is not the same, e.g. one has more products or more addends than the other
    if (container1->type != container2->type
        || container1->coefficient1 != container2->coefficient1
        || container1->coefficient2 != container2->coefficient2
        || container1->list1->num_of_nodes != container2->list1->num_of_nodes
        || container1->list2->num_of_nodes != container2->list2->num_of_nodes)
        return 0;

    // two containers can be identical, i.e. have the same type, number of arguments etc.
    // but the arguments can be in different order, and thus we need to check all the arguments
    // in one container against all the arguments in the other, for this we will make temporary lists
    // of the arguments in the second container and check them against the lists of the first container
    // every match we find we remove from the temp list, else one argument in one the second list
    // can exist more than once in the first list, and we might get a false positive
    // finally we return true if the temp lists are empty
    LinkedList *tempList1 = new_LinkedList();

    // copy the second container's arguments to the first temp list
    for (int i = 1; i <= container2->list1->num_of_nodes; i++) {
        add_to_end_of_LinkedList(tempList1, get_nth_object_in_LinkedList(container2->list1, i));
    }

    // check each arguemnt in the temp list against every argument in the first list
    for (int i = 1; i <= container1->list1->num_of_nodes; i++) {
        // after a complete loop in the temp list, if we don't have a match x will remain 0
        // so we don't bother checking anything else and we return
        int x = 0;
        for (int j = 1; j <= tempList1->num_of_nodes; j++) {
            Argument *temp1 = ((Argument *) get_nth_object_in_LinkedList(container1->list1, i));
            Argument *temp2 = ((Argument *) get_nth_object_in_LinkedList(tempList1, j));

            // skip if one argument is NULL, for whatever reason
            if (temp1 == NULL || temp2 == NULL) {
                printf("** NULL ARGUMENT **");
                continue;
            }
            // TODO: instead of doing the work, maybe we can just call sameArgument instead
            if (temp1->argType == CONST_LETTER || temp1->argType == VAR) {
                if (temp1->arg.str[0] == temp2->arg.str[0]) {
                    x = 1;
                    remove_nth_object_in_LinkedList(tempList1, j);
                    j--;
                    break;
                }
            } else if (temp1->argType == CONTAINER) {
                if (sameContainer(temp1->arg.container, temp2->arg.container)) {
                    x = 1;
                    remove_nth_object_in_LinkedList(tempList1, j);
                    j--;
                    break;
                }
            } else if (temp1->argType == FUNC) {
                if (sameFunc(temp1->arg.func, temp2->arg.func)) {
                    x = 1;
                    remove_nth_object_in_LinkedList(tempList1, j);
                    j--;
                    break;
                }
            }
        }
        // if even a single argument in the first container does not match
        // an argument in the second container we return false
        if (!x) {
            //delete_LinkedList(tempList1);
            return 0;
        }
    }

    // if the list is not empty, then an argument in the second container does not match
    // another in the first list and also we return false
    if (!LinkedList_is_empty(tempList1)) {
        //delete_LinkedList(tempList1);
        return 0;
    }

    // this part is similar as the part above only we do the same stuff to the second lists of both containers
    // TODO: maybe even we can remove this part, put the whole thing inside another function and just call it twice
    LinkedList *tempList2 = new_LinkedList();

    for (int i = 1; i <= container2->list2->num_of_nodes; i++) {
        add_to_end_of_LinkedList(tempList2, get_nth_object_in_LinkedList(container2->list2, i));
    }

    for (int i = 1; i <= container1->list2->num_of_nodes; i++) {
        int x = 0;
        for (int j = 1; j <= tempList2->num_of_nodes; j++) {
            Argument *temp1 = ((Argument *) get_nth_object_in_LinkedList(container1->list2, i));
            Argument *temp2 = ((Argument *) get_nth_object_in_LinkedList(tempList2, j));

            if (temp1 == NULL || temp2 == NULL) {
                printf("** NULL ARGUMENT **");
                continue;
            }

            if (temp1->argType == CONST_LETTER || temp1->argType == VAR) {
                if (temp1->arg.str[0] == temp2->arg.str[0]) {
                    x = 1;
                    remove_nth_object_in_LinkedList(tempList2, j);
                    j--;
                    break;
                }
            } else if (temp1->argType == CONTAINER) {
                if (sameContainer(temp1->arg.container, temp2->arg.container)) {
                    x = 1;
                    remove_nth_object_in_LinkedList(tempList2, j);
                    j--;
                    break;
                }
            } else if (temp1->argType == FUNC) {
                if (sameFunc(temp1->arg.func, temp2->arg.func)) {
                    x = 1;
                    remove_nth_object_in_LinkedList(tempList2, j);
                    j--;
                    break;
                }
            }
        }
        if (!x) {
            //delete_LinkedList(tempList1);
            //delete_LinkedList(tempList2);
            return 0;
        }
    }

    if (!LinkedList_is_empty(tempList1)) {
        //delete_LinkedList(tempList1);
        //delete_LinkedList(tempList2);
        return 0;
    }

    //delete_LinkedList(tempList1);
    //delete_LinkedList(tempList2);

    // after doing all that, if everything match we return true
    return 1;
}

// this function should remove any argument that exists in both list of an argument
// e.g. x/x = 1, m-m = 0
void optimizeDuplicate(Argument **arg)
{
    if (arg == NULL)
        return;

    Container *container = NULL;

    // this checks first if the argument really containes a container
    if ((*arg)->argType == CONTAINER) {
        container = (*arg)->arg.container;
    } else {
        printf("** CAN'T OPTIMIZE THIS ARGUMENT **");
        return;
    }

    // if a list is NULL or empty we can't remove anything
    if (container->list1 == NULL || container->list2 == NULL
            || LinkedList_is_empty(container->list1) || LinkedList_is_empty(container->list2)) {
        //printf("Container is missing a list or more\n");
        return;
    }
    /*
    else {
        printf("list1: %d, list2: %d\n", container->list1->num_of_nodes, container->list2->num_of_nodes);
    }
    */

    // we need to check every argument in the first list against every argument in the second list
    // if any argument matches any other arguement we remove them
    for (int i = 1; i <= container->list1->num_of_nodes; i++) {
        for (int j = 1; j <= container->list2->num_of_nodes; j++) {
            Argument *temp1 = ((Argument *) get_nth_object_in_LinkedList(container->list1, i));
            Argument *temp2 = ((Argument *) get_nth_object_in_LinkedList(container->list2, j));

            // if any arguemnt is NULL, we skip it
            if (temp1 == NULL || temp2 == NULL) {
                printf("** NULL ARGUMENT **");
                continue;
            }

            // TODO: this part can be removed, and we can just call sameArg
            if (temp1->argType == temp2->argType) {
                if (temp1->argType == CONST_LETTER || temp1->argType == VAR) {
                    if (temp1->arg.str[0] == temp2->arg.str[0]) {
                        //printf("char: %c will be removed\n", temp1->arg.str[0]);
                        remove_nth_object_in_LinkedList(container->list1, i);
                        remove_nth_object_in_LinkedList(container->list2, j);
                        i--;
                        j--;
                    }
                } else if (temp1->argType == CONTAINER) {
                    //optimizeDuplicate();
                    //optimizeDuplicate(&temp2);
                    //temp1 = optimizer(temp1, 0);
                    //temp2 = optimizer(temp2, 0);
                    //print_Argument(temp1, 1);
                    //print_Argument(temp2, 1);
                    if (sameContainer(temp1->arg.container, temp2->arg.container)) {
                        //printf("A container will be removed\n");
                        remove_nth_object_in_LinkedList(container->list1, i);
                        remove_nth_object_in_LinkedList(container->list2, j);
                        i--;
                        j--;
                    }
                } else if (temp1->argType == FUNC) {
                    if (sameFunc(temp1->arg.func, temp2->arg.func)) {
                        //printf("A container will be removed\n");
                        remove_nth_object_in_LinkedList(container->list1, i);
                        remove_nth_object_in_LinkedList(container->list2, j);
                        i--;
                        j--;
                    }
                }
                /*
                else if (temp1->argType == CONST) {
                    if (temp1->arg.dbl == temp2->arg.dbl) {
                        remove_nth_object_in_LinkedList(container->list1, i);
                        remove_nth_object_in_LinkedList(container->list2, j);
                        i--;
                        j--;
                    }
                }
                */
                else {
                    printf("** UNSUPPORTED ARGUMENT TYPE ** %d", temp1->argType);
                }
            }
        }
    }
    if (LinkedList_is_empty(container->list1) && LinkedList_is_empty(container->list2)) {
        if (container->type == CSUM) {
            printf("ZERO CONTAINER\n");
            *arg = new_ArgConst(container->coefficient1);
        }
        /* else if (container->type == CMUL) {
            printf("ONE CONTAINER\n");
            *arg = new_ArgConst(container->coefficient1 / container->coefficient2);
        }
        */
    }
}

// this function should take two lists and add all addends to the pos list
// and all subtrahends to the neg list
// TODO: this function needs better documentation
int recSum(Function *input, LinkedList *pos, LinkedList *neg)
{
    // if the the argument is a letter or a number we just add it to the list
    if (input->arg1->argType == CONST || input->arg1->argType == CONST_LETTER
            || input->arg1->argType == VAR) {
        add_to_end_of_LinkedList(pos, input->arg1);
    } else if (input->arg1->argType == FUNC
            && (input->arg1->arg.func->funcType == SUM
                || input->arg1->arg.func->funcType == DIFF)) {
        // if the argument is a function, if the function is also addition of subtraction
        // we call recSum recursively and we give it the same lists, so that the other
        // stuff gets added also to that same list
        if (recSum(input->arg1->arg.func, pos, neg))
            return -1;
    } else if (input->arg1->argType == FUNC) {
        // if the argument is a function with another type, we then need to add the whole
        // function as a sub list
        // like (2 + 3 + 6 + (4 * 7)) should become something like      /+/ | 2 | 3 | 6 | * |
        //                                                                                |
        //                                                                               /*/ | 4 | 7 |
        // so we need to call the optimizer and add the returned list to our list
        Argument *temp = optimize(input->arg1->arg.func);
        if (temp == NULL)
            return -1;
        else
            add_to_end_of_LinkedList(pos, temp);
    }

    // now for the other list, we do the same thing but if the function type is negative
    // we flip the list ordering so that the every two negatives cancel each other
    // so that (2 + 4 - (-3)) should become something like      /+/ | 2 | 4 | 3 |
    if (input->funcType == SUM) {
        if (input->arg2->argType == CONST || input->arg2->argType == CONST_LETTER
                || input->arg2->argType == VAR) {
            add_to_end_of_LinkedList(pos, input->arg2);
        } else if (input->arg2->argType == FUNC
                && (input->arg2->arg.func->funcType == SUM
                    || input->arg2->arg.func->funcType == DIFF)) {
            if (recSum(input->arg2->arg.func, pos, neg))
                return -1;
        } else if (input->arg2->argType == FUNC) {
            Argument *temp = optimize(input->arg2->arg.func);
            if (temp == NULL)
                return -1;
            else
                add_to_end_of_LinkedList(pos, temp);
        }
    } else if (input->funcType == DIFF) {
        if (input->arg2->argType == CONST || input->arg2->argType == CONST_LETTER
                || input->arg2->argType == VAR) {
            add_to_end_of_LinkedList(neg, input->arg2);
        } else if (input->arg2->argType == FUNC
                && (input->arg2->arg.func->funcType == SUM
                    || input->arg2->arg.func->funcType == DIFF)) {
            if (recSum(input->arg2->arg.func, neg, pos))
                return -1;
        } else if (input->arg2->argType == FUNC) {
            Argument *temp = optimize(input->arg2->arg.func);
            if (temp == NULL)
                return -1;
            else
                add_to_end_of_LinkedList(neg, temp);
        }
    } else {
        printf("can't be other than sum or difference\n");
        return -1;
    }

    return 0;
}

// this function should take two lists and add all products to the nom list
// and all quotients to the denom list
// TODO: this function also needs better documentation
int recProd(Function *input, LinkedList *nom, LinkedList *denom)
{
    if (input->arg1->argType == CONST || input->arg1->argType == CONST_LETTER
            || input->arg1->argType == VAR) {
        add_to_end_of_LinkedList(nom, input->arg1);
    } else if (input->arg1->argType == FUNC
            && (input->arg1->arg.func->funcType == PROD
                || input->arg1->arg.func->funcType == QUOT)) {
        if (recProd(input->arg1->arg.func, nom, denom))
            return -1;
    } else if (input->arg1->argType == FUNC) {
        Argument *temp = optimize(input->arg1->arg.func);
        if (temp == NULL)
            return -1;
        else
            add_to_end_of_LinkedList(nom, temp);
    }

    if (input->funcType == PROD) {
        if (input->arg2->argType == CONST || input->arg2->argType == CONST_LETTER
                || input->arg2->argType == VAR) {
            add_to_end_of_LinkedList(nom, input->arg2);
        } else if (input->arg2->argType == FUNC
                && (input->arg2->arg.func->funcType == PROD
                    || input->arg2->arg.func->funcType == QUOT)) {
            if (recProd(input->arg2->arg.func, nom, denom))
                return -1;
        } else if (input->arg2->argType == FUNC) {
            Argument *temp = optimize(input->arg2->arg.func);
            if (temp == NULL)
                return -1;
            else
            add_to_end_of_LinkedList(nom, temp);
        }
    } else if (input->funcType == QUOT) {
        if (input->arg2->argType == CONST || input->arg2->argType == CONST_LETTER
                || input->arg2->argType == VAR) {
            add_to_end_of_LinkedList(denom, input->arg2);
        } else if (input->arg2->argType == FUNC
                && (input->arg2->arg.func->funcType == PROD
                    || input->arg2->arg.func->funcType == QUOT)) {
            if (recProd(input->arg2->arg.func, denom, nom))
                return -1;
        } else if (input->arg2->argType == FUNC) {
            Argument *temp = optimize(input->arg2->arg.func);
            if (temp == NULL)
                return -1;
            else
            add_to_end_of_LinkedList(denom, temp);
        }
    } else {
        printf("can't be other than product or quotient\n");
        return -1;
    }

    return 0;
}

// this function should take an argument and return it as a container within a container
// an argument like this:     (2 + 5 - 4 - 6 + 7 - (-3))
/* would be represented as something like this:         -
                                                       / \
                                                      /   \
                                                     /     \
                                                    +       -
                                                   / \     / \
                                                  -   7   0   3
                                                 / \
                                                -   6
                                               / \
                                              +   4
                                             / \
                                            2   5
*/
// this function should linearize it as a container like this: /+/ | 2 | 5 | 7 | 3 || 4 | 6 |
// with two lists, the first holding all the positives and the other holding all the negatives
// this makes it very easy to optimize
// if we have something like (2 + (3 * 6)) or (7 + sin(x)) this and recSum
// should take care to add the other container arguments in this container thus:        /+/ | 2 | * |
//                                                                                                |
//                                                                                               /*/ | 3 | 6 |
//
// and                                                                                  /+/ | 7 | * |
/*                                                                                                |
//                                                                                             trigFunc
//                                                                                            /        \
//                                                                                           sin        x
// respectively
*/

// TODO: this function also needs better documentation
Argument *optimizeSum(Function *input)
{
    LinkedList *positives = new_LinkedList();
    LinkedList *negatives = new_LinkedList();

    // we send the input function to recSum, and we give it the lists that we have created
    // and it should call itself recursively until the lists contain all arguments
    if (recSum(input, positives, negatives))
        return NULL;

    double sum = 0;

    // if the first list contains a number we remove it and add it to the sum
    for (int i = 1; i <= positives->num_of_nodes; i++) {
        Argument *temp = ((Argument *) get_nth_object_in_LinkedList(positives, i));

        if (temp->argType == CONST) {
            //printf("%d: adding %0.9g to sum of %0.9g\n", i, temp->arg.dbl, sum);
            sum += temp->arg.dbl;
            remove_nth_object_in_LinkedList(positives, i);
            i--;
        }
    }

    // just like the first list, but we subtract from the sum
    for (int i = 1; i <= negatives->num_of_nodes; i++) {
        Argument *temp = ((Argument *) get_nth_object_in_LinkedList(negatives, i));

        if (temp->argType == CONST) {
            //printf("%d: subtraction %0.9g from sum of %0.9g\n", i, temp->arg.dbl, sum);
            sum -= temp->arg.dbl;
            remove_nth_object_in_LinkedList(negatives, i);
            i--;
        }
    }

    if (LinkedList_is_empty(positives) && LinkedList_is_empty(negatives)) {
    // if the lists are empty, no need to return a container, we just return an argument
        return new_ArgConst(sum);
    } else {
    // else we return a new container
        Argument *container =  new_ArgContainer(new_Container(sum, 0, CSUM, positives, negatives));
        optimizeDuplicate(&container);
        printf("returning value from sum: ");
        print_Argument_out(container, 1);
        return container;
    }
}

// this function is like the optimizeSum but is for multiplication
// TODO: this function also needs better documentation
Argument *optimizeProd(Function *input)
{
    LinkedList *nominator = new_LinkedList();
    LinkedList *denominator = new_LinkedList();

    if (recProd(input, nominator, denominator))
        return NULL;

    double prod = 1;
    double quot = 1;
    int zeroNom = 0, zeroDenom = 0;

    // every thing here is the similar as optimizeSum
    // TODO: maybe we can combine the two function and maybe even combine the two loops
    for (int i = 1; i <= nominator->num_of_nodes; i++) {
        Argument *temp = ((Argument *) get_nth_object_in_LinkedList(nominator, i));

        if (temp->argType == CONST) {
            //printf("%d: muliplying %0.9g by %0.9g\n", i, prod, temp->arg.dbl);
            if (temp->arg.dbl == 0) {
                //printf("nominator has zero\n");
                zeroNom = 1;
            } else {
                prod *= temp->arg.dbl;
                remove_nth_object_in_LinkedList(nominator, i);
                i--;
            }
        }
    }

    for (int i = 1; i <= denominator->num_of_nodes; i++) {
        Argument *temp = ((Argument *) get_nth_object_in_LinkedList(denominator, i));

        if (temp->argType == CONST) {
            //printf("%d: dividing %0.9g by %0.9g\n", i, prod, temp->arg.dbl);
            if (temp->arg.dbl == 0) {
                //printf("denominator has zero\n");
                zeroDenom = 1;
            } else {
                quot *= temp->arg.dbl;
                remove_nth_object_in_LinkedList(denominator, i);
                i--;
            }
        }
    }

    // this is to check if we have 0/something something/0 or 0/0
    if (zeroDenom) {
        // zero ins the denumerator or in both nom and denom is a problem
        if (zeroNom) {
            printf("** UNDETERMINABLE FRACTION **");
            //cleanup(-3);
            return NULL;
        } else {
            printf("** UNDEFINED FRACTION **");
            //cleanup(-3);
            return NULL;
        }
    } else if (zeroNom) {
        // if we have zero in the numerator only, we can ignore everything else
        // and just return a argument containing zero
        return new_ArgConst(0);
    } else if (LinkedList_is_empty(nominator) && LinkedList_is_empty(denominator)) {
        if (quot != 1)
            return new_ArgFunc(new_FuncWithValues(QUOT, new_ArgConst(prod), new_ArgConst(quot)));
        else
            return new_ArgConst(prod);
    }
    Argument *container = new_ArgContainer(new_Container(prod, quot, CMUL, nominator, denominator));
    optimizeDuplicate(&container);
    printf("returning value from prod: ");
    print_Argument_out(container, 1);
    return container;
}

// this function takes a container argument and return a normal argument
Argument *deContainerize(Argument *arg)
{
    if (arg == NULL)
        return NULL;

    Container *container = NULL;

    // this checks first if the argument really containes a container
    if (arg->argType == CONST || arg->argType == VAR || arg->argType == CONST_LETTER) {
        // if the argument is a letter or a number there is nothing to be done
        return arg;
    } else if (arg->argType == FUNC) {
        // if it is a function, depending on the type we may need to decontainerize one argument or both
        if (arg->arg.func->funcType == ZERO) {
            return arg;
        } else if (arg->arg.func->funcType == EXP || arg->arg.func->funcType == LOG) {
            return new_ArgFunc(new_FuncWithValues(arg->arg.func->funcType,
                        deContainerize(arg->arg.func->arg1), deContainerize(arg->arg.func->arg2)));
        } else if (arg->arg.func->funcType == TRIG) {
            return new_ArgFunc(new_FuncWithValues(TRIG, arg->arg.func->arg1, deContainerize(arg->arg.func->arg2)));
        } else {
            printf("** THIS SHOULD NOT BE POSSIBLE **");
        }
    } else if (arg->argType == CONTAINER) {
        // if it really has a container, we put it in the pointer and continue the function
        container = arg->arg.container;
    } else {
        printf("** CAN'T DECONTAINERIZE THIS ARGUMENT **");
    }

    // we need a root argument and a branch and we will descend down the root argument
    Argument *branch = new_ArgEmpty();
    Argument *root = branch;

    // the arguments' types are determind by the container's type
    FUNC_TYPE type1 = NULL_FUNC;
    FUNC_TYPE type2 = NULL_FUNC;

    if (container->type == CSUM) {
        type1 = SUM;
        type2 = DIFF;
    } else if (container->type == CMUL) {
        type1 = PROD;
        type2 = QUOT;
    } else {
        printf("** UNKNOWN CONTAINER TYPE **");
    }

    // we loop through the container first list and we set the root as a new argument
    // having the last root as one of its arguments
    for (int i = 1; i <= container->list1->num_of_nodes; i++) {
        //print_Argument_out(get_nth_object_in_LinkedList(container->list1, i), 1);
        Argument *temp = ((Argument *) get_nth_object_in_LinkedList(container->list1, i));

        if (temp->argType == VAR || temp->argType == CONST_LETTER) {
            root = new_ArgFunc(new_FuncWithValues(type1, root, temp));
            //printf("root is now at %ld and it contains the function at %ld\n", (long) root, (long) root->arg.func);
        } else if (temp->argType == CONTAINER) {
            root = new_ArgFunc(new_FuncWithValues(type1, root, deContainerize(temp)));
            //printf("root is now at %ld and it contains the function at %ld\n", (long) root, (long) root->arg.func);
        } else if (temp->argType == CONST) {
            // DO NOTHING
        } else if (temp->argType == FUNC) {
            if (temp->arg.func->funcType == EXP || temp->arg.func->funcType == LOG) {
                root = new_ArgFunc(new_FuncWithValues(type1, root, new_ArgFunc(new_FuncWithValues(temp->arg.func->funcType,
                                    deContainerize(temp->arg.func->arg1), deContainerize(temp->arg.func->arg2)))));
            } else if (temp->arg.func->funcType == TRIG) {
                root = new_ArgFunc(new_FuncWithValues(type1, root, new_ArgFunc(new_FuncWithValues(TRIG, temp->arg.func->arg1,
                                    deContainerize(temp->arg.func->arg2)))));
            } else {
                printf("** THIS SHOULD NOT BE POSSIBLE **");
            }
            //root = new_ArgFunc(new_FuncWithValues(type1, root, temp));
        } else {
            printf("** WRONG ARGUMENT TYPE **");
        }
    }

    // same as the first list, we do with the second list
    for (int i = 1; i <= container->list2->num_of_nodes; i++) {
        //print_Argument_out(get_nth_object_in_LinkedList(container->list2, i), 1);
        Argument *temp = ((Argument *) get_nth_object_in_LinkedList(container->list2, i));

        if (temp->argType == VAR || temp->argType == CONST_LETTER) {
            root = new_ArgFunc(new_FuncWithValues(type2, root, temp));
            //printf("root is now at %ld and it contains the function at %ld\n", (long) root, (long) root->arg.func);
        } else if (temp->argType == CONTAINER) {
            root = new_ArgFunc(new_FuncWithValues(type2, root, deContainerize(temp)));
            //printf("root is now at %ld and it contains the function at %ld\n", (long) root, (long) root->arg.func);
        } else if (temp->argType == CONST) {
            // DO NOTHING
        } else if (temp->argType == FUNC) {
            if (temp->arg.func->funcType == EXP || temp->arg.func->funcType == LOG) {
                root = new_ArgFunc(new_FuncWithValues(type2, root, new_ArgFunc(new_FuncWithValues(temp->arg.func->funcType,
                                    deContainerize(temp->arg.func->arg1), deContainerize(temp->arg.func->arg2)))));
            } else if (temp->arg.func->funcType == TRIG) {
                root = new_ArgFunc(new_FuncWithValues(type2, root, new_ArgFunc(new_FuncWithValues(TRIG, temp->arg.func->arg1,
                                    deContainerize(temp->arg.func->arg2)))));
            } else {
                printf("** THIS SHOULD NOT BE POSSIBLE **");
            }
            //root = new_ArgFunc(new_FuncWithValues(type2, root, temp));
        } else {
            printf("** WRONG ARGUMENT TYPE **");
        }
    }

    // lastly we set the branch as the container's coefficient and we return it
    branch->argType = FUNC;
    branch->arg.func = new_FuncWithValues(type2, new_ArgConst(container->coefficient1), new_ArgConst(container->coefficient2));

    return root;
}

// this function is redundant and thus never used
// TODO: consider removing this function
/*
void solveContainer(Container *container)
{
    if (container->type == CSUM) {
        for (int i = 1; i <= container->list1->num_of_nodes; i++) {
            Argument *temp = (Argument *) get_nth_object_in_LinkedList(container->list1, i);
            if (temp->argType == CONST) {
                container->coefficient += temp->arg.dbl;
                remove_nth_object_in_LinkedList(container->list1, i);
                i--;
            }
        }
        for (int i = 1; i <= container->list2->num_of_nodes; i++) {
            Argument *temp = (Argument *) get_nth_object_in_LinkedList(container->list2, i);
            if (temp->argType == CONST) {
                container->coefficient -= temp->arg.dbl;
                remove_nth_object_in_LinkedList(container->list2, i);
                i--;
            }
        }
    } else if (container->type == CMUL) {
        for (int i = 1; i <= container->list1->num_of_nodes; i++) {
            Argument *temp = (Argument *) get_nth_object_in_LinkedList(container->list1, i);
            if (temp->argType == CONST) {
                container->coefficient *= temp->arg.dbl;
                remove_nth_object_in_LinkedList(container->list1, i);
                i--;
            }
        }
        for (int i = 1; i <= container->list2->num_of_nodes; i++) {
            Argument *temp = (Argument *) get_nth_object_in_LinkedList(container->list2, i);
            if (temp->argType == CONST) {
                container->coefficient /= temp->arg.dbl;
                remove_nth_object_in_LinkedList(container->list2, i);
                i--;
            }
        }
    }
}
*/

// optimize a function
Argument *optimize(Function *input)
{
    if (input->funcType == SUM || input->funcType == DIFF) {
    // if it is a sum or diff, all optimizeSum
        Argument *temp = optimizeSum(input);
        if (temp == NULL)
            return NULL;
        //printf("Sum after optimization: %0.9g\n", temp->arg.dbl);
        //print_Argument_out(temp, 1);
        return temp;
    } else if (input->funcType == PROD || input->funcType == QUOT) {
    // if it is a prod or quot, all optimizeSum
        Argument *temp = optimizeProd(input);
        if (temp == NULL)
            return NULL;
        //printf("Product after optimization: %0.9g\n", temp->arg.dbl);
        //print_Argument_out(temp, 1);
        return temp;
    } else if (input->funcType == EXP){
        // else call optimizer with the 2 function arguments
        // also x^1 is just x
        if (input->arg2->argType == CONST && input->arg2->arg.dbl == 1)
            return new_ArgValues(input->arg1->argType, (void *) input->arg1->arg.func);
        else {
            Argument *arg1 = optimizer(input->arg1);
            Argument *arg2 = optimizer(input->arg2);
            if (arg1 == NULL || arg2 == NULL)
                return NULL;
            return new_ArgFunc(new_FuncWithValues(input->funcType, arg1, arg2));
        }
    } else if (input->funcType == LOG) {
        // similar to exp
        // TODO: add logx(x) = x
        Argument *arg1 = optimizer(input->arg1);
        Argument *arg2 = optimizer(input->arg2);
        if (arg1 == NULL || arg2 == NULL)
            return NULL;
        return new_ArgFunc(new_FuncWithValues(input->funcType, arg1, arg2));
    } else if (input->funcType == TRIG){
        // trig functin only has the second argument, arg1 is just a type
        return new_ArgFunc(new_trigFunc((POLI_TYPE) input->arg1, optimizer(input->arg2)));
    } else if (input->funcType == ZERO) {
        return new_ArgConst(0);
    } else {
        printf("** DID NOT OPTIMIZE FUNCTION WITH TYPE %d **\n", input->funcType);
        return new_ArgFunc(input->arg1);
    }
}

// optimize an argument, this is the entry point to the optimizer
// TODO: maybe split the optimizer into multiple files and/or make a stand alone library
Argument *optimizer(Argument *input)
{
    if (input == NULL) {
        printf("** CAN'T OPTIMIZE AN ARGUMENT THAT DOES NOT EXIST **\n");
        //cleanup(-4);
        return NULL;
    }

    if (input->argType == FUNC || input->argType == CONTAINER) {
        Argument *temp = optimize(input->arg.func);
        if (temp == NULL)
            return NULL;

        //printf("temp arg type: %d\n", temp->argType);
        //if (temp->argType == CONTAINER) {
            //printf("before\n");
            //print_Argument_out(temp, 1);
            //optimizeDuplicate(&temp);
            //printf("after\n");
            //print_Argument_out(temp, 1);
            //solveContainer(temp->arg.container);
        //}
        //print_Container_out(temp, 1, 0);
        //return deContainerize(temp);
        //if (num) {
            //print_Argument_out(temp, 1);
            //return temp;
        //} else {
            //return temp;
        //if (num) {
            //optimizer(deContainerize(temp), 0);
        //} else {
        //}
        return temp;
    } else {
        return input;
    }
}
