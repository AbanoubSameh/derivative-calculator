/* This file has been created on June-29-2020 */
/* Copyright (c) 2020 Abanoub Sameh */

%{
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "../headers/Function.h"
#include "../headers/solveTrigDer.h"
#define YYSTYPE Argument*
#include "../headers/main.h"
int yylex();
extern int mycode;

void yyerror() {
    //printf("%s\n", str);
    //printf("code: %d\n", mycode);
    if (mycode != -1)
        mycode = -2;
}

int yywrap() {
    return 1;
}

extern Argument *Ast;

%}

%start exp
%right POW

%token NUM END OPR CPR VART DVAR ERR
%token ADD SUB MUL DIV PI E
%token SINT COST TANT ASINT ACOST ATANT
%token SECT CSECT CTANT ASECT ACSECT ACTANT
%token SINHT COSHT TANHT ASINHT ACOSHT ATANHT
%token SECHT CSECHT CTANHT ASECHT ACSECHT ACTANHT
%token POW LOGT LN

%%

function
    : OPR start CPR                     { $$ = $2; }
    | SINT start CPR                    { $$ = new_ArgFunc(new_trigFunc(SIN, $2)); }
    | COST start CPR                    { $$ = new_ArgFunc(new_trigFunc(COS, $2)); }
    | TANT start CPR                    { $$ = new_ArgFunc(new_trigFunc(TAN, $2)); }
    | ASINT start CPR                   { $$ = new_ArgFunc(new_trigFunc(ASIN, $2)); }
    | ACOST start CPR                   { $$ = new_ArgFunc(new_trigFunc(ACOS, $2)); }
    | ATANT start CPR                   { $$ = new_ArgFunc(new_trigFunc(ATAN, $2)); }
    | SECT start CPR                    { $$ = new_ArgFunc(new_trigFunc(SEC, $2)); }
    | CSECT start CPR                   { $$ = new_ArgFunc(new_trigFunc(CSEC, $2)); }
    | CTANT start CPR                   { $$ = new_ArgFunc(new_trigFunc(CTAN, $2)); }
    | ASECT start CPR                   { $$ = new_ArgFunc(new_trigFunc(ASEC, $2)); }
    | ACSECT start CPR                  { $$ = new_ArgFunc(new_trigFunc(ACSEC, $2)); }
    | ACTANT start CPR                  { $$ = new_ArgFunc(new_trigFunc(ACTAN, $2)); }

    | SINHT start CPR                   { $$ = new_ArgFunc(new_trigFunc(SINH, $2)); }
    | COSHT start CPR                   { $$ = new_ArgFunc(new_trigFunc(COSH, $2)); }
    | TANHT start CPR                   { $$ = new_ArgFunc(new_trigFunc(TANH, $2)); }
    | ASINHT start CPR                  { $$ = new_ArgFunc(new_trigFunc(ASINH, $2)); }
    | ACOSHT start CPR                  { $$ = new_ArgFunc(new_trigFunc(ACOSH, $2)); }
    | ATANHT start CPR                  { $$ = new_ArgFunc(new_trigFunc(ATANH, $2)); }
    | SECHT start CPR                   { $$ = new_ArgFunc(new_trigFunc(SECH, $2)); }
    | CSECHT start CPR                  { $$ = new_ArgFunc(new_trigFunc(CSECH, $2)); }
    | CTANHT start CPR                  { $$ = new_ArgFunc(new_trigFunc(CTANH, $2)); }
    | ASECHT start CPR                  { $$ = new_ArgFunc(new_trigFunc(ASECH, $2)); }
    | ACSECHT start CPR                 { $$ = new_ArgFunc(new_trigFunc(ACSECH, $2)); }
    | ACTANHT start CPR                 { $$ = new_ArgFunc(new_trigFunc(ACTANH, $2)); }

    | LOGT NUM OPR start CPR            { $$ = new_ArgFunc(new_FuncWithValues(LOG, $2, $4)); }
    | LOGT VART OPR start CPR           { $$ = new_ArgFunc(new_FuncWithValues(LOG, $2, $4)); }
    | LOGT OPR start CPR                { $$ = new_ArgFunc(new_FuncWithValues(LOG, new_ArgConst(10), $3)); }
    | LN OPR start CPR                  { $$ = new_ArgFunc(new_FuncWithValues(LOG, new_ArgConst(M_E), $3)); }
    | function POW function             { $$ = new_ArgFunc(new_FuncWithValues(EXP, $1, $3)); }
    | PI                                { $$ = new_ArgConst(M_PI); }
    | E                                 { $$ = new_ArgConst(M_E); }
    | VART
    | NUM
    ;

unary
    : SUB function                      { $$ = new_ArgFunc(new_FuncWithValues(DIFF, new_ArgConst(0), $2)); }
    | function
    ;

multiplicative
    : multiplicative MUL unary          { $$ = new_ArgFunc(new_FuncWithValues(PROD, $1, $3)); }
    | multiplicative function           { $$ = new_ArgFunc(new_FuncWithValues(PROD, $1, $2)); }
    | multiplicative DIV unary          { $$ = new_ArgFunc(new_FuncWithValues(QUOT, $1, $3)); }
    | unary
    ;

additive
    : additive ADD multiplicative       { $$ = new_ArgFunc(new_FuncWithValues(SUM, $1, $3)); }
    | additive SUB multiplicative       { $$ = new_ArgFunc(new_FuncWithValues(DIFF, $1, $3)); }
    | multiplicative
    ;

start
    : additive
    ;

exp
    : END                               { Ast = NULL; mycode = -3; return 0; }
    | DVAR END                          { Ast = NULL; mycode = -3; return 0; }
    | start END                         { Ast = $1; return 1; }
    | DVAR start END                    { Ast = $2; return 1; }
    ;

%%
