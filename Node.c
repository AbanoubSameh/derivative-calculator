// This file has been created on March-22-2020
// Copyright (c) 2020 Abanoub Sameh

#include <stdio.h>
#include <stdlib.h>

#include "headers/Node.h"

#include "headers/main.h"
#include "headers/Function.h"
#include "headers/Container.h"

// constructor for Node type
Node *new_Node(void *obj)
{
	Node *node = malloc(sizeof(Node));
    node->obj = obj;
    node->next = NULL;
	return node;
}

// destructor for Node type
int delete_Node(Node *node)
{
    if (node == NULL) {
        printf("can't delete a Node that does not exist\n");
        return 0;
    }

    free(node);
    node = NULL;

    //printf("deleted a node\n");
    return 1;
}

int delete_metaNode(Node *node, int nodeType)
{
    if (node == NULL) {
        printf("can't delete a Node that does not exist\n");
        return 0;
    }

    if (nodeType) {
        //printf("will delete node's object\n");
        if (nodeType == ARGUMENT_T)
            delete_Argument(node->obj);
        else if (nodeType == FUNCTION_T)
            delete_Function(node->obj);
        else if (nodeType == CONTAINER_T)
            delete_Container(node->obj);
        else if (nodeType == LIST_T) {
            delete_LinkedList(node->obj);
        } else if (nodeType == DOUBLE_T) {
            // DO NOTHING
        } else {
            printf("unknown object type\n");
        }
    }

    free(node);
    node = NULL;
    return 1;
}

Node *duplicate_Node(Node *node)
{
    if (node == NULL) {
        printf("can't dupliate a node that does not exist\n");
        return NULL;
    }

    Node *new = malloc(sizeof(Node));
    new->obj = node->obj;
    new->next = node->next;
    return new;
}
