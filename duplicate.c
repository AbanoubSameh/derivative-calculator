// This file has been created on June-22-2020
// Copyright (c) 2020 Abanoub Sameh

#include <stdlib.h>
#include <string.h>

#include "headers/duplicate.h"

// some functions to duplicate stuff
char *strduplic(char* str)
{
    char *new_str = malloc(strlen(str) + 1);
    if (new_str == NULL) {
        return NULL;
    }
    strcpy(new_str, str);
    return new_str;
}

int *intduplic(int *num)
{
    int *new_int = malloc(sizeof(int));
    if (new_int == NULL) {
        return NULL;
    }
    *new_int = *num;
    return new_int;
}

double *dblduplic(double *dbl)
{
    double *new_dbl = malloc(sizeof(double));
    if (new_dbl == NULL) {
        return NULL;
    }
    *new_dbl = *dbl;
    return new_dbl;
}

