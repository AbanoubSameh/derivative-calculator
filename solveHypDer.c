// This file has been created on December-06-2020
// Copyright (c) 2020 Abanoub Sameh

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "headers/Function.h"
#include "headers/solveTrigDer.h"
#include "headers/solveDerivative.h"
#include "headers/main.h"

void solveHypDer(Function *input, Function **result, Argument *deriv)
{
    if ((long) input->arg1 == SINH) {
        (*result)->arg1 = new_ArgFunc(new_trigFunc(COSH, input->arg2));
        (*result)->arg2 = deriv;
    } else if ((long) input->arg1 == COSH) {
        (*result)->arg1 = new_ArgFunc(new_trigFunc(SINH, input->arg2));
        (*result)->arg2 = deriv;
    } else if ((long) input->arg1 == TANH) {
        (*result)->arg1 = new_ArgFunc(new_FuncWithValues(EXP,
                    new_ArgFunc(new_trigFunc(SECH, input->arg2)), new_ArgConst(2)));
        (*result)->arg2 = deriv;
    } else if ((long) input->arg1 == ASINH) {
        Function *rootBase = new_FuncWithValues(SUM, new_ArgFunc(new_FuncWithValues(EXP,
                        input->arg2, new_ArgConst(2))), new_ArgConst(1));
        Function *root = new_FuncWithValues(EXP, new_ArgFunc(rootBase),
                new_ArgFunc(new_FuncWithValues(QUOT, new_ArgConst(1), new_ArgConst(2))));
        (*result) = new_FuncWithValues(PROD,
                new_ArgFunc(new_FuncWithValues(QUOT, new_ArgConst(1), new_ArgFunc(root))), deriv);
    } else if ((long) input->arg1 == ACOSH) {
        Function *rootBase = new_FuncWithValues(DIFF, new_ArgFunc(new_FuncWithValues(EXP,
                        input->arg2, new_ArgConst(2))), new_ArgConst(1));
        Function *root = new_FuncWithValues(EXP, new_ArgFunc(rootBase),
                new_ArgFunc(new_FuncWithValues(QUOT, new_ArgConst(1), new_ArgConst(2))));
        (*result) = new_FuncWithValues(PROD,
                new_ArgFunc(new_FuncWithValues(QUOT, new_ArgConst(1), new_ArgFunc(root))), deriv);
    } else if ((long) input->arg1 == ATANH) {
        Function *denum = new_FuncWithValues(DIFF, new_ArgConst(1),
                new_ArgFunc(new_FuncWithValues(EXP, input->arg2, new_ArgConst(2))));
        Function *total = new_FuncWithValues(QUOT, new_ArgConst(1), new_ArgFunc(denum));
        (*result) = new_FuncWithValues(PROD, new_ArgFunc(total), deriv);
    }
    else
        if ((long) input->arg1 == SECH) {
        Function *temp = new_trigFunc(COSH, input->arg2);
        Argument *solution = solveDerivative(new_ArgFunc(
                    new_FuncWithValues(QUOT, new_ArgConst(-1), new_ArgFunc(temp))));
        (*result) = solution->arg.func;
    } else if ((long) input->arg1 == CSECH) {
        Function *temp = new_trigFunc(SINH, input->arg2);
        Argument *solution = solveDerivative(new_ArgFunc(
                    new_FuncWithValues(QUOT, new_ArgConst(-1), new_ArgFunc(temp))));
        (*result) = solution->arg.func;
    } else if ((long) input->arg1 == CTANH) {
        Function *temp = new_trigFunc(TANH, input->arg2);
        Argument *solution = solveDerivative(new_ArgFunc(
                    new_FuncWithValues(QUOT, new_ArgConst(-1), new_ArgFunc(temp))));
        (*result) = solution->arg.func;
    } else if ((long) input->arg1 == ASECH) {
        Function *temp = new_trigFunc(ACOSH, input->arg2);
        Argument *solution = solveDerivative(new_ArgFunc(
                    new_FuncWithValues(QUOT, new_ArgConst(1), new_ArgFunc(temp))));
        (*result) = solution->arg.func;
    } else if ((long) input->arg1 == ACSECH) {
        Function *temp = new_trigFunc(ASINH, input->arg2);
        Argument *solution = solveDerivative(new_ArgFunc(
                    new_FuncWithValues(QUOT, new_ArgConst(1), new_ArgFunc(temp))));
        (*result) = solution->arg.func;
    } else if ((long) input->arg1 == ACTANH) {
        Function *temp = new_trigFunc(ATANH, input->arg2);
        Argument *solution = solveDerivative(new_ArgFunc(
                    new_FuncWithValues(QUOT, new_ArgConst(1), new_ArgFunc(temp))));
        (*result) = solution->arg.func;
    } else {
        printf("** NO SUCH TRIG FUNCTION **");
    }
}

void printHyp(Function *input, int addNewLine)
{
    if (input == NULL) {
        printf("NULL TRIG FUNCTION\n");
        return;
    }

    POLI_TYPE type = (POLI_TYPE) input->arg1;
    if (type == SINH)
        //printf("sin(");
        strncat(output_buffer, "sinh(", ob_size - strlen(output_buffer));
    else if (type == COSH)
        //printf("cos(");
        strncat(output_buffer, "cosh(", ob_size - strlen(output_buffer));
    else if (type == TANH)
        //printf("tan(");
        strncat(output_buffer, "tanh(", ob_size - strlen(output_buffer));

    else if (type == ASINH)
        //printf("arcsin(");
        strncat(output_buffer, "arcsinh(", ob_size - strlen(output_buffer));
    else if (type == ACOSH)
        //printf("arccos(");
        strncat(output_buffer, "arccosh(", ob_size - strlen(output_buffer));
    else if (type == ATANH)
        //printf("arctan(");
        strncat(output_buffer, "arctanh(", ob_size - strlen(output_buffer));

    else if (type == SECH)
        //printf("sec(");
        strncat(output_buffer, "sech(", ob_size - strlen(output_buffer));
    else if (type == CSECH)
        //printf("cosec(");
        strncat(output_buffer, "cosech(", ob_size - strlen(output_buffer));
    else if (type == CTANH)
        //printf("cotan(");
        strncat(output_buffer, "cotanh(", ob_size - strlen(output_buffer));

    else if (type == ACSECH)
        //printf("arccosec(");
        strncat(output_buffer, "arccosech(", ob_size - strlen(output_buffer));
    else if (type == ASECH)
        //printf("arcsec(");
        strncat(output_buffer, "arcsech(", ob_size - strlen(output_buffer));
    else if (type == ACTANH)
        //printf("arccotan(");
        strncat(output_buffer, "arccotanh(", ob_size - strlen(output_buffer));
    else
        printf("** CAN'T PRINT SUCH TRIG FUNCTION **");

    if (input->arg2 == NULL)
        print_Argument(input->arg2, 0);
    else
        print_Argument(input->arg2, 0);

    //printf(")");
    strncat(output_buffer, ")", ob_size - strlen(output_buffer));

    if (addNewLine)
        //printf("\n");
        strncat(output_buffer, "\n", ob_size - strlen(output_buffer));
}

void printHyp_out(Function *input, int addNewLine)
{
    if (input == NULL) {
        printf("NULL TRIG FUNCTION\n");
        return;
    }

    POLI_TYPE type = (POLI_TYPE) input->arg1;
    if (type == SINH)
        printf("sinh(");
        //strncat(output_buffer, "sin(", ob_size - strlen(output_buffer));
    else if (type == COSH)
        printf("cosh(");
        //strncat(output_buffer, "cos(", ob_size - strlen(output_buffer));
    else if (type == TANH)
        printf("tanh(");
        //strncat(output_buffer, "tan(", ob_size - strlen(output_buffer));

    else if (type == ASINH)
        printf("arcsinh(");
        //strncat(output_buffer, "arcsin(", ob_size - strlen(output_buffer));
    else if (type == ACOSH)
        printf("arccosh(");
        //strncat(output_buffer, "arccos(", ob_size - strlen(output_buffer));
    else if (type == ATANH)
        printf("arctanh(");
        //strncat(output_buffer, "arctan(", ob_size - strlen(output_buffer));

    else if (type == SECH)
        printf("sech(");
        //strncat(output_buffer, "sec(", ob_size - strlen(output_buffer));
    else if (type == CSECH)
        printf("cosech(");
        //strncat(output_buffer, "cosec(", ob_size - strlen(output_buffer));
    else if (type == CTANH)
        printf("cotanh(");
        //strncat(output_buffer, "cotan(", ob_size - strlen(output_buffer));

    else if (type == ACSECH)
        printf("arccosech(");
        //strncat(output_buffer, "arccosec(", ob_size - strlen(output_buffer));
    else if (type == ASECH)
        printf("arcsech(");
        //strncat(output_buffer, "arcsec(", ob_size - strlen(output_buffer));
    else if (type == ACTANH)
        printf("arccotanh(");
        //strncat(output_buffer, "arccotan(", ob_size - strlen(output_buffer));
    else
        printf("** CAN'T PRINT OUT SUCH TRIG FUNCTION **");

    if (input->arg2 == NULL)
        print_Argument_out(input->arg2, 0);
    else
        print_Argument_out(input->arg2, 0);

    printf(")");
    //strncat(output_buffer, ")", ob_size - strlen(output_buffer));

    if (addNewLine)
        printf("\n");
        //strncat(output_buffer, "\n", ob_size - strlen(output_buffer));
}
