// This file has been created on November-22-2020
// Copyright (c) 2020 Abanoub Sameh

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "headers/Node.h"
#include "headers/main.h"
#include "headers/Argument.h"
#include "headers/Container.h"
#include "headers/LinkedList.h"

#include "headers/debug.h"

extern LinkedList *contList;

Container *new_Container(double coefficient1, double coefficient2, CONTAINER_TYPE type, LinkedList *list1, LinkedList *list2)
{
    Container *new = malloc(sizeof(Container));
    new->coefficient1 = coefficient1;
    new->coefficient2 = coefficient2;
    new->type = type;
    new->list1 = list1;
    new->list2 = list2;

    add_to_start_of_LinkedList(contList, new);
    return new;
}

void print_Container(Container *input, int addNewLine, int addBrackets)
{
    if (input == NULL) {
        // error
        printf("** NULL CONTAINER POINTER **");
        return;
    }

    char temp_buffer[1000];

    if (input->type == CSUM) {
        if (addBrackets)
            strncat(output_buffer, "(", ob_size - strlen(output_buffer));

        if (input->coefficient1 != 0 && input->coefficient1 != -1) {
            snprintf(temp_buffer, 1000, "%0.9g", input->coefficient1);
            strncat(output_buffer, temp_buffer, ob_size - strlen(output_buffer));
            if (!LinkedList_is_empty(input->list1))
                strncat(output_buffer, "+", ob_size - strlen(output_buffer));
        }

        for (int i = 1; i <= input->list1->num_of_nodes; i++) {
            if (i != 1)
                strncat(output_buffer, "+", ob_size - strlen(output_buffer));

            Argument *temp = get_nth_object_in_LinkedList(input->list1, i);
            if (temp->argType == CONTAINER)
                print_Container(temp->arg.container, 0, 0);
            else
                print_Argument(temp, 0);
        }

        for (int i = 1; i <= input->list2->num_of_nodes; i++) {
            strncat(output_buffer, "-", ob_size - strlen(output_buffer));

            Argument *temp = get_nth_object_in_LinkedList(input->list2, i);
            if (temp->argType == CONTAINER)
                print_Container(temp->arg.container, 0, 0);
            else
                print_Argument(temp, 0);
        }

        if (addBrackets)
            strncat(output_buffer, ")", ob_size - strlen(output_buffer));

    } else if (input->type == CMUL) {
        //if (input->coefficient1 != 1 || input->coefficient2 != 1 || (input->list1->num_of_nodes == 0 && input->list2->num_of_nodes == 0)) {
        if (input->coefficient1 != 1) {
            snprintf(temp_buffer, 1000, "%0.9g", input->coefficient1);
            strncat(output_buffer, temp_buffer, ob_size - strlen(output_buffer));
        }
        for (int i = 1; i <= input->list1->num_of_nodes; i++) {
            Argument *temp = get_nth_object_in_LinkedList(input->list1, i);
            if (temp->argType == CONTAINER)
                print_Container(temp->arg.container, 0, 0);
            else
                print_Argument(temp, 0);
        }
        if (input->list2->num_of_nodes > 0 || input->coefficient2 != 1) {
            if (input->list1->num_of_nodes == 0 && input->coefficient1 == 1) {
                strncat(output_buffer, "1", ob_size - strlen(output_buffer));
            }

            strncat(output_buffer, "/", ob_size - strlen(output_buffer));

            int cocount = 0;

            if (input->coefficient2 != 1) {
                snprintf(temp_buffer, 1000, "%0.9g", input->coefficient2);
                strncat(output_buffer, temp_buffer, ob_size - strlen(output_buffer));
                cocount = 1;
            }

            if ((input->list2->num_of_nodes + cocount) > 1)
                strncat(output_buffer, "(", ob_size - strlen(output_buffer));

            for (int i = 1; i <= input->list2->num_of_nodes; i++) {
                Argument *temp = get_nth_object_in_LinkedList(input->list2, i);
                if (temp->argType == CONTAINER)
                    print_Container(temp->arg.container, 0, 1);
                else
                    print_Argument(temp, 0);
            }
            if ((input->list2->num_of_nodes + cocount) > 1)
                strncat(output_buffer, ")", ob_size - strlen(output_buffer));
        }
    }

    if (addNewLine)
        strncat(output_buffer, "\n", ob_size - strlen(output_buffer));
}

void print_Container_out(Container *input, int addNewLine, int addBrackets)
{
    if (input == NULL) {
        // error
        printf("** NULL CONTAINER POINTER **");
        return;
    }

    //printf("container: %0.9g, %0.9g\n", input->coefficient1, input->coefficient2);
    if (input->type == CSUM) {
        if (addBrackets)
            printf("(");
        if (input->coefficient1 != 0 || (input->list1->num_of_nodes == 0 && input->list2->num_of_nodes == 0)) {
            printf("%0.9g", input->coefficient1);
            if (!LinkedList_is_empty(input->list1))
                printf("+");
        }
        for (int i = 1; i <= input->list1->num_of_nodes; i++) {
            if (i != 1)
                printf("+");
            Argument *temp = get_nth_object_in_LinkedList(input->list1, i);
            if (temp->argType == CONTAINER)
                print_Container_out(temp->arg.container, 0, 0);
            else
                print_Argument_out(temp, 0);
        }

        for (int i = 1; i <= input->list2->num_of_nodes; i++) {
            printf("-");
            Argument *temp = get_nth_object_in_LinkedList(input->list2, i);
            if (temp->argType == CONTAINER)
                print_Container_out(temp->arg.container, 0, 0);
            else
                print_Argument_out(temp, 0);
        }
        if (addBrackets)
            printf(")");
    } else if (input->type == CMUL) {
        if (input->coefficient1 != 1 || input->coefficient2 != 1 || (input->list1->num_of_nodes == 0 && input->list2->num_of_nodes == 0)) {
            printf("%0.9g", input->coefficient1);
            if (input->coefficient2 != 1)
                printf("/%0.9g", input->coefficient2);
        }
        for (int i = 1; i <= input->list1->num_of_nodes; i++) {
            Argument *temp = get_nth_object_in_LinkedList(input->list1, i);
            if (temp->argType == CONTAINER)
                print_Container_out(temp->arg.container, 0, 1);
            else
                print_Argument_out(temp, 0);
        }
        if (input->list2->num_of_nodes > 0) {
            if (input->list1->num_of_nodes == 0 && input->coefficient1 == 1)
                printf("1");

            printf("/");

            if (input->list2->num_of_nodes > 1)
                printf("(");

            for (int i = 1; i <= input->list2->num_of_nodes; i++) {
                Argument *temp = get_nth_object_in_LinkedList(input->list2, i);
                if (temp->argType == CONTAINER)
                    print_Container_out(temp->arg.container, 0, 1);
                else
                    print_Argument_out(temp, 0);
            }
            if (input->list2->num_of_nodes > 1)
                printf(")");
        }
    }

    if (addNewLine)
        printf("\n");
}

void print_Container_debug(Container *input) {
    printf("|\tc");
    print_id((long) input);
    printf("\t|\tct%d\t|\t%0.9g\t|\t%0.9g\t", input->type, input->coefficient1, input->coefficient2);
    LinkedList *toPrint = new_LinkedList();

    printf("|\tnom\t");
    for (int i = 1; i <= input->list1->num_of_nodes; i++) {
        Argument *temp = get_nth_object_in_LinkedList(input->list1, i);
        if (temp != NULL) {
            printf("|\t");
            Argument *printArg = print_Argument_debug(temp);
            if (printArg != NULL)
                add_to_end_of_LinkedList(toPrint, printArg);
            printf("\t");
        }
    }

    printf("|\tdenom\t");

    for (int i = 1; i <= input->list2->num_of_nodes; i++) {
        Argument *temp = get_nth_object_in_LinkedList(input->list2, i);
        if (temp != NULL) {
            printf("|\t");
            Argument *printArg = print_Argument_debug(temp);
            if (printArg != NULL)
                add_to_end_of_LinkedList(toPrint, printArg);
            printf("\t");
        }
    }

    printf("|\n");

    for (int i = 1; i <= toPrint->num_of_nodes; i++) {
        Argument *temp = get_nth_object_in_LinkedList(toPrint, i);
        if (temp != NULL) {
            print_Argument_debug_content(temp);
        }
    }
}

void delete_Container(Container *input)
{
    if (input != NULL) {
        //if (input->list1 != NULL)
            //delete_LinkedList(input->list1, DO_NOT_DELETE);
        //if (input->list2 != NULL)
            //delete_LinkedList(input->list2, DO_NOT_DELETE);
        free(input);
    } else
        printf("can't delete NULL Container\n");
}
