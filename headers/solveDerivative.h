// This file has been created on June-20-2020
// Copyright (c) 2020 Abanoub Sameh

#ifndef _solvederivative_h_
#define _solvederivative_h_

#include "Function.h"

Argument *solveDerivative(Argument *);
Function *solveFunctionDer(Function *);

#endif
