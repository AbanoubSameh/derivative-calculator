// This file has been created on June-20-2020
// Copyright (c) 2020 Abanoub Sameh

#ifndef _function_h_
#define _function_h_

#include "Argument.h"

typedef enum FUNC_TYPE {
    NULL_FUNC,
    ZERO,

    SUM,
    DIFF,
    PROD,
    QUOT,

    EXP,
    LOG,
    TRIG,
    MOD,
} FUNC_TYPE;

typedef struct Function {
    FUNC_TYPE funcType;
    Argument *arg1;
    Argument *arg2;
} Function;

Function *new_Function();
Function *new_FuncZero();
Function *new_FuncWithValues(FUNC_TYPE, Argument *, Argument *);
void print_Function(Function *, int);
void print_Function_out(Function *, int);
void print_Function_debug(Function *);
Function *Func_duplic(Function *);
void delete_Function(Function *);

#endif
