// This file has been created on June-22-2020
// Copyright (c) 2020 Abanoub Sameh

#ifndef _duplicate_h_
#define _duplicate_h_

char *strduplic(char* str);
int *intduplic(int *num);
double *dblduplic(double *dbl);

#endif
