// This file has been created on June-20-2020
// Copyright (c) 2020 Abanoub Sameh

#ifndef _solveexpder_h_
#define _solveexpder_h_

#include "Function.h"

Function *solveExpDer(Function *);

#endif
