// This file has been created on June-20-2020
// Copyright (c) 2020 Abanoub Sameh

#ifndef _solvealgebder_h_
#define _solvealgebder_h_

#include "Function.h"

Function *solveAlgebDer(Function *);

#endif
