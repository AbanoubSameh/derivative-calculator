// This file has been created on June-20-2020
// Copyright (c) 2020 Abanoub Sameh

#ifndef _argument_h_
#define _argument_h_

typedef enum ARG_TYPE {
    NULL_ARG,
    CONST,
    VAR,
    FUNC,
    CONST_LETTER,
    CONTAINER,
} ARG_TYPE;

typedef union Arg {
    double dbl;
    char *str;
    struct Function *func;
    struct Container *container;
} Arg;

typedef struct Argument {
    ARG_TYPE argType;
    Arg arg;
} Argument;

Argument *new_ArgEmpty();
Argument *new_ArgValues(ARG_TYPE, void *);
Argument *new_ArgConst(double);
Argument *new_ArgConstLetter(char *);
Argument *new_ArgVar(char *);
Argument *new_ArgFunc(void *);
Argument *new_ArgContainer(void *);

void print_Argument(Argument *, int);
void print_Argument_out(Argument *, int);
void *print_Argument_debug(Argument *);
void print_Argument_debug_content(Argument *);
Argument *Arg_duplic(Argument *);
void delete_Argument(Argument *);
int ArgIsConst(Argument *);

#endif
