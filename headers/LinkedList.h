// This file has been created on March-22-2020
// Copyright (c) 2020 Abanoub Sameh

#ifndef _linkedlist_h_
#define _linkedlist_h_

#include "Node.h"

typedef struct LinkedList {
    int num_of_nodes;
    Node *root;
    Node *last;
} LinkedList;

LinkedList *new_LinkedList();
LinkedList *new_metaLinkedList();
int delete_LinkedList(LinkedList*);
int delete_metaLinkedList(LinkedList*, int);

int LinkedList_is_empty(LinkedList*);
int empty_LinkedList(LinkedList**);

int add_to_start_of_LinkedList(LinkedList*, void*);
int add_to_end_of_LinkedList(LinkedList*, void*);

int remove_first_from_LinkedList(LinkedList*);
int remove_last_from_LinkedList(LinkedList*);
int remove_nth_object_in_LinkedList(LinkedList*, int);

void *get_nth_object_in_LinkedList(LinkedList*, int);
void *get_fisrt_object_in_LinkedList(LinkedList*);
void *get_last_object_in_LinkedList(LinkedList*);

#endif
