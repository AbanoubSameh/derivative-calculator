// This file has been created on June-30-2020
// Copyright (c) 2020 Abanoub Sameh

#ifndef _optimizer_h_
#define _optimizer_h_

#include "Function.h"

Argument *deContainerize(Argument *);
Argument *optimize(Function *);
Argument *optimizer(Argument *);

#endif
