// This file has been created on March-22-2020
// Copyright (c) 2020 Abanoub Sameh

#ifndef _node_h_
#define _node_h_

typedef struct Node {
    void *obj;
	struct Node *next;
} Node;

Node *new_Node(void*);
int delete_Node(Node*);
int delete_metaNode(Node*, int);

Node *duplicate_Node(Node*);

#endif
