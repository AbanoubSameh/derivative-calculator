// This file has been created on November-22-2020
// Copyright (c) 2020 Abanoub Sameh

#ifndef _container_h_
#define _container_h_

#include "Function.h"
#include "LinkedList.h"

typedef enum CONTAINER_TYPE { CSUM, CMUL } CONTAINER_TYPE;

typedef struct Container {
    double coefficient1;
    double coefficient2;
    CONTAINER_TYPE type;
    LinkedList *list1;
    LinkedList *list2;
} Container;

Container *new_Container(double, double, CONTAINER_TYPE, LinkedList *, LinkedList *);
void print_Container(Container *, int, int);
void print_Container_out(Container *, int, int);
void print_Container_debug(Container *);
void delete_Container(Container *);

#endif
