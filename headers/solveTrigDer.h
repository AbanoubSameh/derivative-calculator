// This file has been created on June-20-2020
// Copyright (c) 2020 Abanoub Sameh

#ifndef _solvetrigder_h_
#define _solvetrigder_h_

#include "Function.h"

typedef enum POLI_TYPE {
    SIN,
    COS,
    TAN,

    ASIN,
    ACOS,
    ATAN,

    SEC,
    CSEC,
    CTAN,

    ACSEC,
    ASEC,
    ACTAN,

    SINH,
    COSH,
    TANH,

    ASINH,
    ACOSH,
    ATANH,

    SECH,
    CSECH,
    CTANH,

    ACSECH,
    ASECH,
    ACTANH,
} POLI_TYPE;

Function *new_trigFunc(POLI_TYPE, Argument *);
Function *solveTrigDer(Function *);
void printTrig(Function *, int);
void printTrig_out(Function *, int);

#endif
