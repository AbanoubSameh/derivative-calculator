// This file has been created on July-09-2020
// Copyright (c) 2020 Abanoub Sameh

#ifndef _main_h_
#define _main_h_

#define DO_NOT_DELETE   0
#define ARGUMENT_T      1
#define FUNCTION_T      2
#define DOUBLE_T        3
#define CONTAINER_T     4
#define LIST_T          5

void cleanup(int exitCode);
char *calc(char *);
char *readString(char *, int *, int);
void change_dvar(char *);
int withRespectTo(char *);

extern char *output_buffer;
extern int ob_size;

#endif
