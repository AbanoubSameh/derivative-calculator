// This file has been created on December-07-2020
// Copyright (c) 2020 Abanoub Sameh

#include <stdio.h>
#include <stdlib.h>
#include "headers/LinkedList.h"

#define RESET   "\x1b[0m"
#define PRE     "\x1b["
#define POST    "m"

static LinkedList *id_list;
static int init = 0;
static int last_id = 10;

struct Id {
    long ptr;
    int id;
    int color;
};

static struct Id *store(long ptr)
{
    struct Id *temp = malloc(sizeof(struct Id));
    temp->id = last_id;
    last_id += 10;
    temp->ptr = ptr;
    temp->color = rand() % (35 + 1 - 30) + 30;
    add_to_start_of_LinkedList(id_list, temp);

    return temp;
}

static struct Id *get_id(long ptr)
{
    if (!init) {
        init = 1;;
        id_list = new_LinkedList();
        //srand(2359523326);
    }

    for (int i = 1; i < id_list->num_of_nodes; i++) {
        struct Id *temp = ((struct Id *) get_nth_object_in_LinkedList(id_list, i));

        if (ptr == temp->ptr)
            return temp;
    }

    return store(ptr);
}

void print_id(long ptr)
{
    struct Id *temp = get_id(ptr);
    printf(PRE "%d" POST "%d" RESET, temp->color, temp->id);
}
