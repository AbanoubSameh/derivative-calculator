/* This file has been created on June-29-2020 */
/* Copyright (c) 2020 Abanoub Sameh */

%{
#include <stdio.h>
#include <stdlib.h>
#include "../headers/Function.h"
#define YYSTYPE Argument*
#include "parser.h"
#include "../headers/main.h"
#undef YY_INPUT
#define YY_INPUT(b, r, s) readString(b, &r, s);
extern int mycode;
%}

%option noinput
%option nounput

%%

[0-9]+              { yylval = new_ArgConst(atof(yytext)); return NUM; }
[0-9]*\.[0-9]+      { yylval = new_ArgConst(atof(yytext)); return NUM; }

\+                  { return ADD; }
\-                  { return SUB; }
\*                  { return MUL; }
\/                  { return DIV; }
[\n]                { return END; }
\(                  { return OPR; }
\)                  { return CPR; }

"sin("              { return SINT; }
"cos("              { return COST; }
"tan("              { return TANT; }
"asin("             { return ASINT; }
"acos("             { return ACOST; }
"atan("             { return ATANT; }

"sec("              { return SECT; }
"csec("             { return CSECT; }
"ctan("             { return CTANT; }
"asec("             { return ASECT; }
"acsec("            { return ACSECT; }
"actan("            { return ACTANT; }

"sinh("             { return SINHT; }
"cosh("             { return COSHT; }
"tanh("             { return TANHT; }
"asinh("            { return ASINHT; }
"acosh("            { return ACOSHT; }
"atanh("            { return ATANHT; }

"sech("             { return SECHT; }
"csech("            { return CSECHT; }
"ctanh("            { return CTANHT; }
"asech("            { return ASECHT; }
"acsech("           { return ACSECHT; }
"actanh("           { return ACTANHT; }

"PI"|"pi"           { return PI; }
"E"|"e"             { return E; }
"^"                 { return POW; }
"log"               { return LOGT; }
"ln"                { return LN; }

\:[a-zA-Z]          { change_dvar(yytext + 1); return DVAR; }
[a-zA-Z]            { if (withRespectTo(yytext)) { yylval = new_ArgVar(yytext); return VART; }
                        else { yylval = new_ArgConstLetter(yytext); return NUM; } }

[ \t]               { /* do nothing */ }
[^0-9]              { /* printf("wrong input: \"%c\"\tbin: %d\n", yytext[0], yytext[0]); */ mycode = -1; return ERR; }

%%
