// This file has been created on June-20-2020
// Copyright (c) 2020 Abanoub Sameh

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "headers/solveTrigDer.h"
#include "headers/solveDerivative.h"
#include "headers/main.h"

Function *solveHypDer(Function *, Function **, Argument *);
void printHyp(Function *, int);
void printHyp_out(Function *, int);

Function *new_trigFunc(POLI_TYPE type, Argument *arg)
{
    Function *new = new_Function();
    new->funcType = TRIG;
    new->arg1 = (void *) type;
    new->arg2 = arg;
    return new;
}

Function *solveTrigDer(Function *input)
{
    Function *result = new_Function();
    result->funcType = PROD;
    Argument *deriv = solveDerivative(input->arg2);

    if ((long) input->arg1 == SIN) {
        result->arg1 = new_ArgFunc(new_trigFunc(COS, input->arg2));
        result->arg2 = deriv;
    } else if ((long) input->arg1 == COS) {
        result->arg1 = new_ArgConst(-1);
        result->arg2 = new_ArgFunc(new_FuncWithValues(PROD,
                    new_ArgFunc(new_trigFunc(SIN, input->arg2)), deriv));
    } else if ((long) input->arg1 == TAN) {
        result->arg1 = new_ArgFunc(new_FuncWithValues(EXP,
                    new_ArgFunc(new_trigFunc(SEC, input->arg2)), new_ArgConst(2)));
        result->arg2 = deriv;
    } else if ((long) input->arg1 == ASIN) {
        Function *rootBase = new_FuncWithValues(DIFF, new_ArgConst(1),
                new_ArgFunc(new_FuncWithValues(EXP, input->arg2, new_ArgConst(2))));
        Function *root = new_FuncWithValues(EXP, new_ArgFunc(rootBase),
                new_ArgFunc(new_FuncWithValues(QUOT, new_ArgConst(1), new_ArgConst(2))));
        result = new_FuncWithValues(PROD,
                new_ArgFunc(new_FuncWithValues(QUOT, new_ArgConst(1), new_ArgFunc(root))), deriv);
    } else if ((long) input->arg1 == ACOS) {
        Function *temp = new_trigFunc(ASIN, input->arg2);
        result = new_FuncWithValues(PROD, new_ArgConst(-1), new_ArgFunc(solveTrigDer(temp)));
    } else if ((long) input->arg1 == ATAN) {
        Function *denum = new_FuncWithValues(SUM,
                new_ArgFunc(new_FuncWithValues(EXP, input->arg2, new_ArgConst(2))), new_ArgConst(1));
        Function *total = new_FuncWithValues(QUOT, new_ArgConst(1), new_ArgFunc(denum));
        result = new_FuncWithValues(PROD, new_ArgFunc(total), deriv);
    }
    else
        if ((long) input->arg1 == SEC) {
        Function *temp = new_trigFunc(COS, input->arg2);
        Argument *solution = solveDerivative(new_ArgFunc(
                    new_FuncWithValues(QUOT, new_ArgConst(-1), new_ArgFunc(temp))));
        result = solution->arg.func;
    } else if ((long) input->arg1 == CSEC) {
        Function *temp = new_trigFunc(SIN, input->arg2);
        Argument *solution = solveDerivative(new_ArgFunc(
                    new_FuncWithValues(QUOT, new_ArgConst(-1), new_ArgFunc(temp))));
        result = solution->arg.func;
    } else if ((long) input->arg1 == CTAN) {
        Function *temp = new_trigFunc(TAN, input->arg2);
        Argument *solution = solveDerivative(new_ArgFunc(
                    new_FuncWithValues(QUOT, new_ArgConst(-1), new_ArgFunc(temp))));
        result = solution->arg.func;
    } else if ((long) input->arg1 == ASEC) {
        Function *temp = new_trigFunc(ACOS, input->arg2);
        Argument *solution = solveDerivative(new_ArgFunc(
                    new_FuncWithValues(QUOT, new_ArgConst(1), new_ArgFunc(temp))));
        result = solution->arg.func;
    } else if ((long) input->arg1 == ACSEC) {
        Function *temp = new_trigFunc(ASIN, input->arg2);
        Argument *solution = solveDerivative(new_ArgFunc(
                    new_FuncWithValues(QUOT, new_ArgConst(1), new_ArgFunc(temp))));
        result = solution->arg.func;
    } else if ((long) input->arg1 == ACTAN) {
        Function *temp = new_trigFunc(ATAN, input->arg2);
        Argument *solution = solveDerivative(new_ArgFunc(
                    new_FuncWithValues(QUOT, new_ArgConst(1), new_ArgFunc(temp))));
        result = solution->arg.func;
    } else {
        solveHypDer(input, &result, deriv);
    }

    return result;
}

void printTrig(Function *input, int addNewLine)
{
    if (input == NULL) {
        printf("NULL TRIG FUNCTION\n");
        return;
    }

    POLI_TYPE type = (POLI_TYPE) input->arg1;
    if (type == SIN)
        //printf("sin(");
        strncat(output_buffer, "sin(", ob_size - strlen(output_buffer));
    else if (type == COS)
        //printf("cos(");
        strncat(output_buffer, "cos(", ob_size - strlen(output_buffer));
    else if (type == TAN)
        //printf("tan(");
        strncat(output_buffer, "tan(", ob_size - strlen(output_buffer));

    else if (type == ASIN)
        //printf("arcsin(");
        strncat(output_buffer, "arcsin(", ob_size - strlen(output_buffer));
    else if (type == ACOS)
        //printf("arccos(");
        strncat(output_buffer, "arccos(", ob_size - strlen(output_buffer));
    else if (type == ATAN)
        //printf("arctan(");
        strncat(output_buffer, "arctan(", ob_size - strlen(output_buffer));

    else if (type == SEC)
        //printf("sec(");
        strncat(output_buffer, "sec(", ob_size - strlen(output_buffer));
    else if (type == CSEC)
        //printf("cosec(");
        strncat(output_buffer, "cosec(", ob_size - strlen(output_buffer));
    else if (type == CTAN)
        //printf("cotan(");
        strncat(output_buffer, "cotan(", ob_size - strlen(output_buffer));

    else if (type == ACSEC)
        //printf("arccosec(");
        strncat(output_buffer, "arccosec(", ob_size - strlen(output_buffer));
    else if (type == ASEC)
        //printf("arcsec(");
        strncat(output_buffer, "arcsec(", ob_size - strlen(output_buffer));
    else if (type == ACTAN)
        //printf("arccotan(");
        strncat(output_buffer, "arccotan(", ob_size - strlen(output_buffer));
    else {
        printHyp(input, addNewLine);
        return;
    }

    if (input->arg2 == NULL)
        print_Argument(input->arg2, 0);
    else
        print_Argument(input->arg2, 0);

    //printf(")");
    strncat(output_buffer, ")", ob_size - strlen(output_buffer));

    if (addNewLine)
        //printf("\n");
        strncat(output_buffer, "\n", ob_size - strlen(output_buffer));
}

void printTrig_out(Function *input, int addNewLine)
{
    if (input == NULL) {
        printf("NULL TRIG FUNCTION\n");
        return;
    }

    POLI_TYPE type = (POLI_TYPE) input->arg1;

    if (type == SIN)
        printf("sin(");
        //strncat(output_buffer, "sin(", ob_size - strlen(output_buffer));
    else if (type == COS)
        printf("cos(");
        //strncat(output_buffer, "cos(", ob_size - strlen(output_buffer));
    else if (type == TAN)
        printf("tan(");
        //strncat(output_buffer, "tan(", ob_size - strlen(output_buffer));

    else if (type == ASIN)
        printf("arcsin(");
        //strncat(output_buffer, "arcsin(", ob_size - strlen(output_buffer));
    else if (type == ACOS)
        printf("arccos(");
        //strncat(output_buffer, "arccos(", ob_size - strlen(output_buffer));
    else if (type == ATAN)
        printf("arctan(");
        //strncat(output_buffer, "arctan(", ob_size - strlen(output_buffer));

    else if (type == SEC)
        printf("sec(");
        //strncat(output_buffer, "sec(", ob_size - strlen(output_buffer));
    else if (type == CSEC)
        printf("cosec(");
        //strncat(output_buffer, "cosec(", ob_size - strlen(output_buffer));
    else if (type == CTAN)
        printf("cotan(");
        //strncat(output_buffer, "cotan(", ob_size - strlen(output_buffer));

    else if (type == ACSEC)
        printf("arccosec(");
        //strncat(output_buffer, "arccosec(", ob_size - strlen(output_buffer));
    else if (type == ASEC)
        printf("arcsec(");
        //strncat(output_buffer, "arcsec(", ob_size - strlen(output_buffer));
    else if (type == ACTAN)
        printf("arccotan(");
        //strncat(output_buffer, "arccotan(", ob_size - strlen(output_buffer));
    else {
        printHyp_out(input, addNewLine);
        return;
    }

    if (input->arg2 == NULL)
        print_Argument_out(input->arg2, 0);
    else
        print_Argument_out(input->arg2, 0);

    printf(")");
    //strncat(output_buffer, ")", ob_size - strlen(output_buffer));

    if (addNewLine)
        printf("\n");
        //strncat(output_buffer, "\n", ob_size - strlen(output_buffer));
}
