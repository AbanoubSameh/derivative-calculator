// This file has been created on June-20-2020
// Copyright (c) 2020 Abanoub Sameh

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "headers/solveDerivative.h"

Function *solveLogDer(Function *input)
{
    if (ArgIsConst(input->arg1)) {
        Function *root = new_FuncWithValues(QUOT, new_ArgConst(1), input->arg2);
        Function *total = new_FuncWithValues(PROD, new_ArgFunc(root), solveDerivative(input->arg2));
        //print_Function(root, 1);
        if (input->arg1->arg.dbl == M_E) {
            return total;
        } else {
            Function *ln = new_FuncWithValues(QUOT, new_ArgConst(1),
                    new_ArgFunc(new_FuncWithValues(LOG, new_ArgConst(M_E), input->arg1)));
            Function *fin = new_FuncWithValues(PROD, new_ArgFunc(total), new_ArgFunc(ln));
            return fin;
        }
    } else {
        Function *num = new_FuncWithValues(LOG, new_ArgConst(M_E), input->arg2);
        Function *denum = new_FuncWithValues(LOG, new_ArgConst(M_E), input->arg1);
        Function *div = new_FuncWithValues(QUOT, new_ArgFunc(num), new_ArgFunc(denum));
        Argument *result = solveDerivative(new_ArgFunc(div));
        return result->arg.func;
    }

    return NULL;
}
