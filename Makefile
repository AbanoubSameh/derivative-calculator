# This file has been created on June-20-2020
# Copyright (c) 2020 Abanoub Sameh

# default compiler
CC=cc

# defaut file names
OUTFILE=derivatives

# other compilers
#DEFCC = gcc
#ALTCC = clang
#WINCC = x86_64-w64-mingw32-gcc

# default cc flags
DEFCFLAGS = -Wall -Wextra -g -O0
RELEASECFLAGS = -Wall -Wextra -O3 -march=znver1 -mtune=znver1

# bulid type
BUILD=debug

ifeq ($(BUILD), debug)
	CFLAGS = $(DEFCFLAGS)
endif
ifeq ($(BUILD), release)
	CFLAGS = $(RELEASECFLAGS)
endif

# libraries
LDFLAGS = -lm

# default locations
SRC = .
OBJ = objects
HDR = headers
GEN = generated

# files to be compiled
CFILES = $(wildcard $(SRC)/*.c)
OFILES = $(CFILES:$(SRC)/%.c=$(OBJ)/%.o)

.PHONY: default directories clean cleanall rebuild

# targets
default: directories $(OUTFILE)

$(OUTFILE): $(OFILES) $(OBJ)/parser.o $(OBJ)/lexer.o
	@$(CC) $(CFLAGS) $(LDFLAGS) -o $(OUTFILE) $(OFILES) $(OBJ)/parser.o $(OBJ)/lexer.o
	@echo "  LD 		$(OUTFILE)"
	@echo ""
	@echo "$(BUILD) build of $(OUTFILE) done"

$(OBJ)/%.o: $(SRC)/%.c $(GEN)/parser.c $(GEN)/lexer.c
	@$(CC) $(CFLAGS) -o $@ -c $<
	@echo "  CC 		$@"

directories: $(GEN) $(OBJ)

$(GEN):
	@[ -d $(GEN) ] || { mkdir $(GEN) && echo "  MKDIR		$(GEN)"; }

$(OBJ):
	@[ -d $(OBJ) ] || { mkdir $(OBJ) && echo "  MKDIR		$(OBJ)"; }

$(GEN)/parser.c: grammar.y
	@yacc -d -o $(GEN)/parser.c $(SRC)/grammar.y
	@echo "  YACC		$@"

$(GEN)/lexer.c: tokens.l $(GEN)/parser.c
	@lex -o $(GEN)/lexer.c $(SRC)/tokens.l
	@echo "  LEX		$@"

$(OBJ)/parser.o: $(GEN)/parser.c
	@$(CC) $(CFLAGS) -o $(OBJ)/parser.o -c $(GEN)/parser.c
	@echo "  CC 		$@"

$(OBJ)/lexer.o: $(GEN)/lexer.c
	@$(CC) $(CFLAGS) -o $(OBJ)/lexer.o -c $(GEN)/lexer.c
	@echo "  CC 		$@"

clean:
	@rm -rf $(GEN)/* $(OBJ)/*
	@echo "  CLEAN		$(GEN)"
	@echo "  CLEAN		$(OBJ)"

cleanall: clean
	@rm -f derivative derivatives derivative.exe derivatives.exe
	@echo "  CLEAN		$(OUTFILE)"
	@rm -rf $(GEN) $(OBJ)

rebuild: clean default

memcheck: derivatives
	valgrind --leak-check=full --show-leak-kinds=all -s ./derivatives
