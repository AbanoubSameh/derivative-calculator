// This file has been created on June-20-2020
// Copyright (c) 2020 Abanoub Sameh

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "headers/Function.h"
#include "headers/solveTrigDer.h"

#include "headers/LinkedList.h"
#include "headers/main.h"

#include "headers/debug.h"

extern LinkedList *funcList;

Function *new_Function()
{
    Function *new = malloc(sizeof(Function));
    //printf("new empty function created at %ld\n", (long) new);
    new->funcType = NULL_FUNC;
    new->arg1 = NULL;
    new->arg2 = NULL;

    add_to_start_of_LinkedList(funcList, new);
    return new;
}

Function *new_FuncZero()
{
    Function *new = malloc(sizeof(Function));
    //printf("new function created at %ld with type ZERO\n", (long) new);
    new->funcType = ZERO;
    new->arg1 = NULL;
    new->arg2 = NULL;

    add_to_start_of_LinkedList(funcList, new);
    return new;
}

Function *new_FuncWithValues(FUNC_TYPE type, Argument *arg1, Argument *arg2)
{
    Function *new = malloc(sizeof(Function));
    //printf("new function created at %ld with type %d\n", (long) new, type);
    new->funcType = type;
    new->arg1 = arg1;
    new->arg2 = arg2;

    add_to_start_of_LinkedList(funcList, new);
    return new;
}

void print_Function(Function *input, int addNewLine)
{
    //printf("printing function\n");
    if (input == NULL) {
        // error
        printf("** NULL FUNCTION POINTER **");
        return;
    }
    if (input->funcType == NULL_FUNC) {
        // error
        printf("** CAN'T PRINT NULL FUNCTION **");
        return;
    }

    if (input->funcType == ZERO) {
        //printf("0");
        strncat(output_buffer, "0", ob_size - strlen(output_buffer));
    } else if (input->funcType == SUM) {
        //printf("(");
        strncat(output_buffer, "(", ob_size - strlen(output_buffer));
        print_Argument(input->arg1, 0);
        //printf("+");
        strncat(output_buffer, "+", ob_size - strlen(output_buffer));
        print_Argument(input->arg2, 0);
        //printf(")");
        strncat(output_buffer, ")", ob_size - strlen(output_buffer));
    } else if (input->funcType == DIFF) {
        //printf("(");
        strncat(output_buffer, "(", ob_size - strlen(output_buffer));
        print_Argument(input->arg1, 0);
        //printf("-");
        strncat(output_buffer, "-", ob_size - strlen(output_buffer));
        print_Argument(input->arg2, 0);
        //printf(")");
        strncat(output_buffer, ")", ob_size - strlen(output_buffer));
    } else if (input->funcType == PROD) {
        //printf("(");
        strncat(output_buffer, "(", ob_size - strlen(output_buffer));
        print_Argument(input->arg1, 0);
        //printf("*");
        strncat(output_buffer, "*", ob_size - strlen(output_buffer));
        print_Argument(input->arg2, 0);
        //printf(")");
        strncat(output_buffer, ")", ob_size - strlen(output_buffer));
    } else if (input->funcType == QUOT) {
        //printf("(");
        strncat(output_buffer, "(", ob_size - strlen(output_buffer));
        print_Argument(input->arg1, 0);
        //printf("/");
        strncat(output_buffer, "/", ob_size - strlen(output_buffer));
        print_Argument(input->arg2, 0);
        //printf(")");
        strncat(output_buffer, ")", ob_size - strlen(output_buffer));
    } else if (input->funcType == EXP) {
        //printf("(");
        strncat(output_buffer, "(", ob_size - strlen(output_buffer));
        print_Argument(input->arg1, 0);
        //printf("^");
        strncat(output_buffer, "^", ob_size - strlen(output_buffer));
        print_Argument(input->arg2, 0);
        //printf(")");
        strncat(output_buffer, ")", ob_size - strlen(output_buffer));
    } else if (input->funcType == LOG) {

        if (ArgIsConst(input->arg1)
                && input->arg1->arg.dbl == M_E) {
            //printf("ln");
            strncat(output_buffer, "ln", ob_size - strlen(output_buffer));
        } else {
            //printf("log");
            strncat(output_buffer, "log", ob_size - strlen(output_buffer));
            print_Argument(input->arg1, 0);
        }

        //printf("(");
        strncat(output_buffer, "(", ob_size - strlen(output_buffer));
        print_Argument(input->arg2, 0);
        //printf(")");
        strncat(output_buffer, ")", ob_size - strlen(output_buffer));
    } else if (input->funcType == TRIG) {
        printTrig(input, 0);
    } else {
        printf("** NO SUCH FUNCTION TYPE **");
    }

    if (addNewLine) {
        //printf("\n");
        strncat(output_buffer, "\n", ob_size - strlen(output_buffer));
    }
}

void print_Function_out(Function *input, int addNewLine)
{
    //printf("printing function\n");
    if (input == NULL) {
        // error
        printf("** NULL FUNCTION POINTER **");
        return;
    }
    if (input->funcType == NULL_FUNC) {
        // error
        printf("** CAN'T PRINT OUT NULL FUNCTION **");
        return;
    }

    if (input->funcType == ZERO) {
        printf("0");
        //strncat(output_buffer, "0", ob_size - strlen(output_buffer));
    } else if (input->funcType == SUM) {
        printf("(");
        //strncat(output_buffer, "(", ob_size - strlen(output_buffer));
        print_Argument_out(input->arg1, 0);
        printf("+");
        //strncat(output_buffer, "+", ob_size - strlen(output_buffer));
        print_Argument_out(input->arg2, 0);
        printf(")");
        //strncat(output_buffer, ")", ob_size - strlen(output_buffer));
    } else if (input->funcType == DIFF) {
        printf("(");
        //strncat(output_buffer, "(", ob_size - strlen(output_buffer));
        print_Argument_out(input->arg1, 0);
        printf("-");
        //strncat(output_buffer, "-", ob_size - strlen(output_buffer));
        print_Argument_out(input->arg2, 0);
        printf(")");
        //strncat(output_buffer, ")", ob_size - strlen(output_buffer));
    } else if (input->funcType == PROD) {
        printf("(");
        //strncat(output_buffer, "(", ob_size - strlen(output_buffer));
        print_Argument_out(input->arg1, 0);
        printf("*");
        //strncat(output_buffer, "*", ob_size - strlen(output_buffer));
        print_Argument_out(input->arg2, 0);
        printf(")");
        //strncat(output_buffer, ")", ob_size - strlen(output_buffer));
    } else if (input->funcType == QUOT) {
        printf("(");
        //strncat(output_buffer, "(", ob_size - strlen(output_buffer));
        print_Argument_out(input->arg1, 0);
        printf("/");
        //strncat(output_buffer, "/", ob_size - strlen(output_buffer));
        print_Argument_out(input->arg2, 0);
        printf(")");
        //strncat(output_buffer, ")", ob_size - strlen(output_buffer));
    } else if (input->funcType == EXP) {
        printf("(");
        //strncat(output_buffer, "(", ob_size - strlen(output_buffer));
        print_Argument_out(input->arg1, 0);
        printf("^");
        //strncat(output_buffer, "^", ob_size - strlen(output_buffer));
        print_Argument_out(input->arg2, 0);
        printf(")");
        //strncat(output_buffer, ")", ob_size - strlen(output_buffer));
    } else if (input->funcType == LOG) {

        if (ArgIsConst(input->arg1)
                && input->arg1->arg.dbl == M_E) {
            printf("ln");
            //strncat(output_buffer, "ln", ob_size - strlen(output_buffer));
        } else {
            printf("log");
            //strncat(output_buffer, "log", ob_size - strlen(output_buffer));
            print_Argument_out(input->arg1, 0);
        }

        printf("(");
        //strncat(output_buffer, "(", ob_size - strlen(output_buffer));
        print_Argument_out(input->arg2, 0);
        printf(")");
        //strncat(output_buffer, ")", ob_size - strlen(output_buffer));
    } else if (input->funcType == TRIG) {
        printTrig_out(input, 0);
    } else {
        printf("** NO SUCH FUNCTION TYPE **");
    }

    if (addNewLine) {
        printf("\n");
        //strncat(output_buffer, "\n", ob_size - strlen(output_buffer));
    }
}

void print_Function_debug(Function *func)
{
    printf("|\tf");
    print_id((long) func);
    printf("\t");

    Argument *temp1 = NULL;
    Argument *temp2 = NULL;

    if (func->funcType == TRIG) {
        printf("|\ttt%ld\t|\t", (long) func->arg1);

        temp2 = print_Argument_debug(func->arg2);
        printf("\t");

    } else {
        printf("|\tft%ld\t|\t", (long) func->funcType);

        temp1 = print_Argument_debug(func->arg1);
        printf("\t|\t");
        temp2 = print_Argument_debug(func->arg2);
        printf("\t");
    }

    printf("|\n");

    if (temp1 != NULL) {
        print_Argument_debug_content(temp1);
        if (temp2 != NULL)
            printf("|\t");
    }

    if (temp2 != NULL)
        print_Argument_debug_content(temp2);

    //printf("|\n\n");
}

Function *Func_duplic(Function *input)
{
    Function *dup = new_Function();
    dup->funcType = input->funcType;

    if (input->funcType != TRIG)
        dup->arg1 = Arg_duplic(input->arg1);
    else
        dup->arg1 = input->arg1;

    dup->arg2 = Arg_duplic(input->arg2);
    return dup;
}

void delete_Function(Function *input)
{
    //printf("freeing a function at %ld\n", (long) input);
    if (input != NULL)
        free(input);
    else
        printf("can't delete NULL Function\n");
}
